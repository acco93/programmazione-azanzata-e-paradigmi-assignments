package ass4.es2;

/**
 * The thread that draws an animation.
 * A drawable must be wrapped at least into a StaticAnimation to be drawn.
 * @author acco
 *
 */
public class Drawer extends Thread {

	private static final long SLEEP_TIME = 200;
	private IAnimation animation;

	public Drawer(IAnimation animation) {
		this.animation = animation;
	}

	@Override
	public void run() {
		
		while(true){

			this.animation.draw();
			
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		

	}

	
	
}
