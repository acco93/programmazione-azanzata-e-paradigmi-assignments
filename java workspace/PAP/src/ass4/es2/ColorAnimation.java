package ass4.es2;

import java.util.Random;

import ass4.es2.TextLib.Color;

/**
 * AbstractAnimation decorator. It causes random color changes on the animation object.
 * 
 * @author acco
 *
 */

public class ColorAnimation extends AbstractAnimation {

	private AbstractAnimation animation;
	private Random random;
	private Drawable drawable;

	ColorAnimation(AbstractAnimation animation){
		this.animation = animation;
		this.drawable = this.animation.getDrawable();
		this.random = new Random();
	}
	
	@Override
	protected void render() {
		this.animation.render();
	}

	@Override
	protected void clear() {
		this.animation.clear();
	}

	@Override
	protected void transform() {
		this.animation.transform();
		if(random.nextBoolean()){
			switch(random.nextInt(8)){
			case 0:
				this.drawable.setColor(Color.BLACK);
				break;
			case 1:
				this.drawable.setColor(Color.BLUE);

				break;
			case 2:
				this.drawable.setColor(Color.CYAN);

				break;
			case 3:
				this.drawable.setColor(Color.GREEN);

				break;
			case 4:
				this.drawable.setColor(Color.MAGENTA);

				break;
			case 5:
				this.drawable.setColor(Color.RED);

				break;
			case 6:
				this.drawable.setColor(Color.WHITE);

				break;
			case 7:
				this.drawable.setColor(Color.YELLOW);
				break;
			}
		} 
	}

	@Override
	protected Drawable getDrawable() {
		return this.drawable;
	}
	

}
