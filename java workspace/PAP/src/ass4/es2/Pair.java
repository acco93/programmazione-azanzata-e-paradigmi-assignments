package ass4.es2;

/**
 * Generic object pair.
 * @author acco
 *
 * @param <T> first field's type
 * @param <U> second field's type
 */
public class Pair <T,U> {
	private T firstField;
	private U secondField;
	
	Pair (T firstField, U secondField){
		this.firstField = firstField;
		this.secondField = secondField;
	}
	
	public T first () { 
		return this.firstField;
	}
	
	public U second () {
		return this.secondField;
	}
	
}
