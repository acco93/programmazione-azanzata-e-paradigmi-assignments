package ass4.es1;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.IntStream;

/**
 * A simple combination generator. It sequentially generates strings 
 * 			from
 * 					[first, min, min, min, ..., min]
 * 			to 	
 * 					[last, max, max, max, ..., max]
 * 
 * @author acco
 *
 */
public class SeqStringGenerator implements Iterator<String>, Iterable<String> {

	// smallest usable char
	private char min;
	// biggest usable char
	private char max;
	// first char to be used (used to split)
	private char first;
	// last char to be used (used to split)
	private char last;
	// string length
	private int len;
	// the string
	private StringBuilder string;
	// true if all the possible strings have been generated
	private boolean noMoreStrings;


	SeqStringGenerator(char min, char max, char first, char last, int len){
		this.min = min;
		this.max = max;
		this.first = first;
		this.last = last;
		this.len = len;
		
		// generate the first string as: [first, min, min, ..., min]
		this.string = new StringBuilder();
		this.string.append(first);
		IntStream.range(0, len-1).forEach(i->this.string.append(min));
		
		
		// some bounds check
		if(this.max>=this.min && this.last>=this.first&&len>0){
			this.noMoreStrings = false;
		} else {
			this.noMoreStrings = true;
		}
		
	
	}
	
	
	@Override
	public String next(){
		
		if(noMoreStrings){
			throw new NoSuchElementException();
		}
		
		String retString = this.string.toString();
		
		// len-1 perchè gli indici partono da 0
		// string[len-1] = aaaaaaa(a) è l'a tra parentesi
		this.inc(this.len-1);
		
		return retString;
		
	}
	
	@Override
	public boolean hasNext() {
		return !this.noMoreStrings;
	}

	/**
	 * Recursively increments the string working with its single chars.
	 * 
	 * @param pos char index to inc
	 */
	private void inc(int pos) {
		
		if(pos == -1) {
			// the most significant char has exceeded max
			this.noMoreStrings = true;
			return;
		}
		
		char c = this.string.charAt(pos);
		
		if(c+1>this.getLast(pos)){
			// the inc char exceeded the admissible one
			// inc the char in pos - 1 
			this.string.setCharAt(pos, min);
			this.inc(pos-1);
		} else {
			this.string.setCharAt(pos, (char)(c+1));
		}
		
	}


	/**
	 * Returns the last admissible char for the given position. For the:
	 * - 1st char [position 0] => max
	 * - otherwise => last
	 * 
	 * @param pos char pos
	 * @return
	 */
	private int getLast(int pos) {
		if(pos==0){
			return this.last;
		} else {
			return this.max;
		}
	}


	// returns the iterator (just to try to work with the streams)
	@Override
	public Iterator<String> iterator() {
		return this;
	}



	
}
