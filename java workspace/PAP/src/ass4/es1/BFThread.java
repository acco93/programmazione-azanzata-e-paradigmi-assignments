package ass4.es1;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * BruteForce thread.
 * Try to log into the system using the strings from the given generator.
 * @author acco
 *
 */
public class BFThread extends Thread{

	//private SeqStringGenerator stringGenerator;
	private SecureSystem sys;
	private Stream<String> stringStream;
	private long startTime;

	public BFThread(SeqStringGenerator s, SecureSystem sys, long startTime) {
		//this.stringGenerator = s;
		this.sys = sys;
		// creo uno stream a partire dall'iteratore SeqStringGenerator
		// uno spliterator è "An object for traversing and partitioning elements of a source"
		// il false indica non parallelo
		this.stringStream = StreamSupport.stream(s.spliterator(), false);
		this.startTime = startTime;
	}

	@Override
	public void run() {
		/*
		 * Without stream
		 * 
		 * while(this.stringGenerator.hasNext()){
			String s = this.stringGenerator.next();
			if(sys.login(s)){
				System.out.println("Found: "+s);
				System.exit(0);
			}
		}*/
		
		this.stringStream.forEach(s -> {
			if(sys.login(s)){
				System.out.println("Found: "+s);
				System.out.println("Execution time: "+(System.currentTimeMillis()-this.startTime)+" msec");
				System.exit(0);
			}
		});
	}
	
	

}
