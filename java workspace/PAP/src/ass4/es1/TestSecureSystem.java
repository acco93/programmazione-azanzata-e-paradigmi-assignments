package ass4.es1;

public class TestSecureSystem {

	public static void main(String[] args) {

		// first printable char
		final char fpc = 32;
		// last printable char
		final char lpc = 126;

		final int passwordLen = 5;

		final int procs = Runtime.getRuntime().availableProcessors() + 1;

		final int charsRange = lpc - fpc;

		final int charsPerThread = (int) Math.ceil((double) charsRange / procs);

		System.out.println("Password length: " + passwordLen);
		System.out.println("Available processors: " + procs);
		System.out.println("Total strings: " + (long) Math.pow(charsRange+1, passwordLen));
		System.out.println("Strings per thread: ~" + (long) (Math.pow(charsRange, passwordLen) / procs));

		// used to split chars per thread
		char currentChar = fpc;

		SecureSystem sys = new SecureSystem(passwordLen);

		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < procs - 1; i++) {

			SeqStringGenerator s = new SeqStringGenerator(fpc, lpc, currentChar, (char) (currentChar + charsPerThread), passwordLen);

			new BFThread(s, sys,startTime).start();

			currentChar += charsPerThread + 1;
		}


		SeqStringGenerator s = new SeqStringGenerator(fpc, lpc, currentChar, lpc, passwordLen);
		new BFThread(s, sys, startTime).start();

		
	}

}
