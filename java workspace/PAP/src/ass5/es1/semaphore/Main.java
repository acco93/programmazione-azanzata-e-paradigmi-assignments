package ass5.es1.semaphore;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Semaphore;


public class Main {

	private static final int MUTEX = 1;
	private static final int EVENT = 0;
	static Space space;
	
	public static void main(String[] args) {
			
		long startTime;
		long endTime;
		
		
		if(args.length>0){
			int n = Integer.parseInt(args[0]);
			space = new Space(n);
		} else {
			space = new Space();
		}
		
		
		System.out.println("== MULTI THREADED VERSION (SEMAPHORE) ==");
		startTime = System.currentTimeMillis();
		Main.multiThreadedVersion();
		endTime = System.currentTimeMillis();		
		System.out.println("\tElapsed time: "+ (endTime-startTime)+" ms");
		System.out.println("== SEQUENTIAL STREAM VERSION ==");
		startTime = System.currentTimeMillis();
		Main.sequentialStreamVersion();
		endTime = System.currentTimeMillis();
		System.out.println("\tElapsed time: "+ (endTime-startTime)+" ms");
	}

	private static void sequentialStreamVersion() {
		
		List<Point> points = space.getPoints();
		Pair<Double, Double> acc = new Pair<>(0.0,0.0);
	
		acc = points.stream()
				// non posso evitare la map perchè la reduce vuole il tipo dell'aggregato uguale a quello della lista
				.map(p->new Pair<Double,Double>(p.getX(),p.getY()))
				// acc è il valore iniziale
				// pSum = point sum
				// nElem = next elem
				.reduce(acc, (pSum, nElem)->new Pair<Double,Double>(pSum.first()+nElem.first(),pSum.second()+nElem.second()));
		
		Point centroid = new Point(acc.first()/points.size(), acc.second()/points.size());
		

		Optional<Point> nearest = points.stream().min((a,b)->Double.compare(Space.d(a,centroid), Space.d(b,centroid)));
		
		System.out.println("\tCentroid: "+ centroid);		
		System.out.println("\tNearest point: "+nearest.get());
		System.out.println("\tDistance: "+Space.d(nearest.get(), centroid));
		
	}

	private static void multiThreadedVersion() {

		final int procs = Runtime.getRuntime().availableProcessors() + 1;
		
		Semaphore mutex = new Semaphore(MUTEX);
		Semaphore[] sync = new Semaphore[procs];
		
		SharedData sd = new SharedData(procs, space);
		
		for(int i=0;i<procs;i++){
			sync[i] = new Semaphore(EVENT);
		}
		
		List<Point> points = space.getPoints();
		
		int pointsPerThread = points.size()/procs;
		
		// lower bound
		int lb = 0;
		// upper bound
		int ub = pointsPerThread;
		
		Worker[] w = new Worker[procs];

		for(int i=0;i<procs;i++){
			
			if(i==procs-1){
				ub = points.size();
			}
			
			// set of semaphores used as sync point (barrier)
			// semaforo dove aspettare (tante volte quanti sono i workers - 1 cioè me stesso)
			Semaphore toWait = sync[i];
			// semaforo dove gli altri aspetteranno => che quindi io faccio signal!
			Semaphore[] toSignal = new Semaphore[procs-1];
			int sIndex = 0;
			for(int j=0;j<procs;j++){
				if(j==i){continue;}
				toSignal[sIndex++]=sync[j];
			}
			
			w[i] = new Worker(lb, ub, sd, mutex, toWait, toSignal);
			w[i].start();
			lb = ub;
			ub += pointsPerThread;
		}
		
		for(int i=0;i<procs;i++){
			try {
				w[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("\tCentroid: "+ sd.getCentroid().get());
		System.out.println("\tNearest point: "+sd.getNearest().get());
		System.out.println("\tDistance: "+Space.d(sd.getNearest().get(),sd.getCentroid().get() ));
		
	}
	
}
