package ass5.es1.semaphore;

import java.util.Optional;

/**
 * 
 * Thread's shared data.
 * Non thread safe! => sync nei worker
 * @author acco
 *
 */

public class SharedData {

	private Pair<Double,Double> sum;
	private Optional<Point> nearest;
	private Optional<Point> centroid;
	private int procs;
	private Space space;
	
	SharedData(int procs, Space space){
		this.procs = procs;
		this.space = space;
		this.sum = new Pair<>(0.0,0.0);
		this.nearest = Optional.empty();
		this.centroid = Optional.empty();
	}
	
	public void incSum(Pair<Double,Double> value){
		double x = this.sum.first()+value.first();
		double y = this.sum.second()+value.second();
		this.sum = new Pair<>(x,y);
		procs--;
		if(procs==0){
			int n = this.space.getPoints().size();
			double cx = this.sum.first()/n;
			double cy = this.sum.second()/n;
			this.centroid = Optional.of(new Point(cx,cy));
		}
	}
	
	public void setNearest(Point pNearest){
		if(!this.nearest.isPresent()){
			this.nearest = Optional.of(pNearest);			
		} else {
			if(Space.d(pNearest, this.centroid.get())<Space.d(this.nearest.get(), this.centroid.get())){
				this.nearest = Optional.of(pNearest);
			}
		}

	}

	public Pair<Double, Double> getSum() {
		return sum;
	}

	public Optional<Point> getNearest() {
		return nearest;
	}

	public Optional<Point> getCentroid() {
		return centroid;
	}
	
	public Space getSpace() {
		return this.space;
	}

	
	
}
