package ass5.es2.lambda;

/**
 * Simple behavior functional interface.
 * @author acco
 *
 */

public interface Behavior {

	void run();

}
