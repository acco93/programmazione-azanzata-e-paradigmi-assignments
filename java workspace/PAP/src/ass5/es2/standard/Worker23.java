package ass5.es2.standard;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Worker23 extends Worker {

	private UnsafeCounter counter;
	private Semaphore wait;
	private Semaphore signal;



	public Worker23(String name, UnsafeCounter c, List<Semaphore> toWait, List<Semaphore> toSignal) {
		super(name);
		this.counter = c;
		this.wait = toWait.get(0);
		this.signal = toSignal.get(0);
	}



	@Override
	public void action() {

		info("Waiting for w1");
		try {
			this.wait.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		info("Incrementing the counter");
		this.counter.inc();

		info("Signaling w4/w5");
		this.signal.release();

	}

}
