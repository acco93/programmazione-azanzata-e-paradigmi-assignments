package ass5.es2.standard;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main {

	private static final int COUNTERS = 4;
	private static final int THREADS = 6;
	private static final int SEMAPHORES = 7;
	
	private static final int MUTEX = 1;
	private static final int EVENT = 0;
	
	private static final int W1 = 0, W2 = 1, W3 = 2, W4 = 3, W5 = 4, W6 = 5;
	private static final int C1 = 0, C2 = 1, C3 = 2, C4 = 3;
	private static final int S1 = 0, S2 = 1, S3 = 2, S4 = 3, S5 = 4, S6 = 5, S45 = 6;

	
	
	static List<Semaphore> toWait;
	static List<Semaphore> toSignal;
		
	public static void main(String[] args) {
		
		boolean verbose = false;
		
		if(args.length>0){
			if(args[0].equals("-log")){
				verbose = true;
			} else {
				System.err.println("Undefined flag: "+args[0]);
			}
		}
		
		Worker[] w = new Worker[THREADS];

		UnsafeCounter[] c = new UnsafeCounter[COUNTERS];
		for(int i=0;i<COUNTERS;i++){
			c[i] = new UnsafeCounter(0);
		}
		
		Semaphore[] s = new Semaphore[SEMAPHORES];

		// w1 WAITs until w6 prints c4
		// w6 prints c4 then SIGNALs w1
		s[S1] = new Semaphore(EVENT);
		// w1 increments c1 then SIGNALs w2 & w3
		// w2 & w3 WAITs until w1 increment c1
		s[S2] = new Semaphore(EVENT);
		s[S3] = new Semaphore(EVENT);
		// w2 & w3 increment c2 & c3 then SIGNAL w4 & w5
		// w4 & w4 WAIT until w2 & w4 increment c2 & c3 then print c2 & c3
		// then concurrently update c4 and SIGNAL w6
		s[S4] = new Semaphore(EVENT);
		s[S5] = new Semaphore(EVENT);
		s[S6] = new Semaphore(EVENT);
		s[S45] = new Semaphore(MUTEX);

		
		// worker 1 
		resetLists();
		toWait.add(s[S1]);
		toSignal.add(s[S2]);
		toSignal.add(s[S3]);
		w[W1] = new Worker1("w1", c[C1], toWait, toSignal);
		
		// worker 2
		resetLists();
		toWait.add(s[S2]);
		toSignal.add(s[S4]);
		w[W2] = new Worker23("w2", c[C2], toWait, toSignal);

		// worker 3
		resetLists();
		toWait.add(s[S3]);
		toSignal.add(s[S5]);
		w[W3] = new Worker23("w3", c[C3], toWait, toSignal);
		
		// worker 4
		resetLists();
		toWait.add(s[S4]);
		toSignal.add(s[S6]);
		w[W4] = new Worker45("w4", c[C2], toWait, toSignal, s[S45], c[C4]);
		
		// worker 5
		resetLists();
		toWait.add(s[S5]);
		toSignal.add(s[S6]);
		w[W5] = new Worker45("w5", c[C3], toWait, toSignal, s[S45], c[C4]);
		
		// worker 6
		resetLists();
		toWait.add(s[S6]);
		toSignal.add(s[S1]);
		w[W6] = new Worker6("w6", c[C4], toWait, toSignal);
		
		for(int i=0;i<THREADS;i++){
			w[i].setVerboseExecution(verbose);
		}
		
		for(int i=0;i<THREADS;i++){
			w[i].start();
		}
		
	}



	private static void resetLists() {
		toWait = new ArrayList<>();
		toSignal = new ArrayList<>();
	}

}
