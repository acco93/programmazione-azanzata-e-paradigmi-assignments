package ass5.es2.standard;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Worker6 extends Worker {



	private Semaphore wait;
	private Semaphore signal;
	private UnsafeCounter cToPrint;

	public Worker6(String name, UnsafeCounter cToPrint, List<Semaphore> toWait, List<Semaphore> toSignal) {
		super(name);
		this.cToPrint = cToPrint;
		this.wait = toWait.get(0);
		this.signal = toSignal.get(0);
	}

	@Override
	public void action() {
		
		try {
			info("Waiting for w4");
			this.wait.acquire();
			info("Waiting for w5");
			this.wait.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		print("Counter value: "+cToPrint.getValue());
		
		info("Signaling w1");
		this.signal.release();

	}

	
}
