package ass6.es2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Optional;

import javax.swing.JPanel;

/**
 * 
 * The drawing panel.
 * 
 * @author acco
 *
 */

public class DrawPanel extends JPanel implements ComponentListener {

	private static final long serialVersionUID = 1L;
	private Optional<WorldState> state;
	private FormView formView;

	// x, y matrix multiplier
	private int posX;
	private int posY;

	private int cellWidth;
	private int cellHeight;

	public DrawPanel(FormView formView) {
		this.formView = formView;
		this.setBackground(Color.black);
		this.addComponentListener(this);
		this.state = Optional.empty();

	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		

		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		g2.setColor(Color.WHITE);

		
		state.ifPresent(s->{
			this.formView.changeProcessingInfo("Generation " + s.getGeneration() + " [" + s.getLivingCells() + "]");

			for (int i = 0; i < World.ROWS; i++) {
				for (int j = 0; j < World.COLUMNS; j++) {
					if (s.get(i, j) == CellState.ALIVE) {
						g2.fillOval(i * posX, j * posY, cellWidth, cellHeight);
					}

				}
			}
	
		});
		
		
	 }

	

	public void setState(WorldState state) {
		this.state = Optional.of(state);
	}

	@Override
	public void componentResized(ComponentEvent e) {

		int w = this.getWidth();
		int h = this.getHeight();

		posX = (int) Math.ceil((double) w / World.ROWS);
		posY = (int) Math.ceil((double) h / World.COLUMNS);

		cellWidth = (int) Math.ceil((double) posX - 0.2 * posX);
		cellHeight = (int) Math.ceil((double) posY - 0.2 * posY);

	}

	@Override
	public void componentMoved(ComponentEvent e) {

	}

	@Override
	public void componentShown(ComponentEvent e) {
		
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	
	}

}
