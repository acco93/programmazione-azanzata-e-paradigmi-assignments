package ass6.es2;

/**
 * 
 * Initial configuration example.
 * 
 * @author acco
 *
 */

public class GliderWorldState extends WorldState {

	public GliderWorldState(int generation) {
		super(generation);
		
		int x = World.ROWS/2 - 3;
		int y = World.COLUMNS/2 - 2;
		
		this.alive(x+2, y+0);
		this.alive(x+3, y+1);
		this.alive(x+3, y+2);
		this.alive(x+2, y+2);
		this.alive(x+1, y+2);
		
		this.setLivingCells(5);
	}

}
