package ass6.es2;

import java.awt.Color;

/**
 * MVC view interface.
 * @author acco
 *
 */
public interface View {

	void draw(WorldState state);
	
	void changeBufferLabel(String text, Color color);
	
	void changeStateLabel(String text, Color color);
}
