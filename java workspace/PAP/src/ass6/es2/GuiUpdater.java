package ass6.es2;

import java.util.concurrent.Semaphore;

/**
 * Thread that takes the next world state from the buffer and
 * asks the EDT to draw it.
 * @author acco
 *
 */
public class GuiUpdater extends Thread {

	private View view;
	private World world;

	private Semaphore sem;
	private SharedAppInfo sai;

	public GuiUpdater(View view, World world, SharedAppInfo sai) {
		this.view = view;
		this.world = world;
		this.sai = sai;
		this.sem = new Semaphore(0);
	}

	@Override
	public void run() {
		while(!sai.isStopped()){
			WorldState state = this.world.get();
			this.view.draw(state);
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			while(sai.isPaused()){
				try {
					this.sem.acquire();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public void pleaseResume(){
		this.sem.release();
	}
	

}
