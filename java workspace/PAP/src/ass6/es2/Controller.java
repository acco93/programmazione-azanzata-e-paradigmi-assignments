package ass6.es2;

import java.awt.Color;

/**
 * 
 * Controller class (MVC pattern).
 * 
 * @author acco
 *
 */

public class Controller implements BufferListener, StateListener{

	
	
	
	private View gui;
	private World world;
	private GuiUpdater guiUpdater;
	private Master master;
	private SharedAppInfo sai;



	public Controller(){
		sai = new SharedAppInfo();
		this.world = new World(World.ROWS, World.COLUMNS);
		gui = new FormView(this);
		master = new Master(world, sai);
		guiUpdater = new GuiUpdater(gui, world, sai);
		master.addBufferListener(this);
		master.addStateListener(this);
		//gui.draw(world.get());
		
	}



	@Override
	public void bufferNotification(int bufferElems) {
		double percentage = ((double)bufferElems/World.BUFFER_CAPACITY)*100;
		
		int val = (int) ((percentage - 0) * (255 - 0) / (100 - 0) + 0);
		Color color = new Color(255-val, 200, val);

			gui.changeBufferLabel("Buffering ... "+String.format("%.2f", percentage)+" %"+ " ["+bufferElems+"/"+World.BUFFER_CAPACITY+"]", color);	

		
		
	}



	public void start() {
		if(this.sai.isStopped()){
			sai = new SharedAppInfo();
			this.world = new World(World.ROWS, World.COLUMNS);
			master = new Master(world, sai);
			guiUpdater = new GuiUpdater(gui, world, sai);
			master.addBufferListener(this);
			master.addStateListener(this);
		}
		master.start();
		guiUpdater.start();
		this.gui.changeStateLabel("Rendering ...", Color.green);
	}



	public void stop() {
		this.gui.changeStateLabel("Attempting graceful shut down ...", Color.yellow);
		// set the stop flag to true and ...
		this.sai.setStopped(true);
		// ... wake up the threads in case they're waiting on condition variables in world.
		this.world.wakeEveryOne();
	}


	public void pauseResume(){
		if(this.sai.isPaused()){
			this.sai.setPaused(false);
			this.guiUpdater.pleaseResume();
			this.gui.changeStateLabel("Rendering ...", Color.green);
		} else {
			this.sai.setPaused(true);
			this.gui.changeStateLabel("Paused ...", Color.yellow);
		}

	}
	

	@Override
	public void stateNotification(ProgramState ps) {
		
		switch(ps){
		case RUNNING:
			this.gui.changeStateLabel("Running ...", Color.green);
			break;
		case STOPPED:
			this.gui.changeStateLabel("Stopped! Bye Bye :)", Color.red);
			break;
		}
		
		
		
	}
	
}
