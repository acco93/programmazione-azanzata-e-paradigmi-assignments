package ass6.es2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * 
 * Bounded buffer of world states.
 * 
 * @author acco
 *
 */

public class World {
	public static final int ROWS = 300;
	public static final int COLUMNS = 150;
	public static final int BUFFER_CAPACITY = 500;
	private WorldState[] states;
	private int first;
	private int last;
	private int count;
	private Lock mutex;
	private Condition notFull;
	private Condition notEmpty;

	public World(int rows, int columns) {
		this.first = 0;
		this.last = 0;
		this.count = 0;
		this.states = new WorldState[BUFFER_CAPACITY];
		
		this.mutex = new ReentrantLock();
		this.notFull = this.mutex.newCondition();
		this.notEmpty = this.mutex.newCondition();

	}

	public WorldState get() {
		try {
			this.mutex.lock();

			if (count == 0) {
				this.notEmpty.await();
			} else {

				first = (first + 1) % BUFFER_CAPACITY;
				count--;
				this.notFull.signal();

			}

		} catch (InterruptedException e) {
		
			e.printStackTrace();
		} finally {
			this.mutex.unlock();
		}
		// ci potrebbe essere un problema se viene svegliato per terminare e non ci sono stati
		// in verità no perchè tanto i gli stati passati non vengono eliminati bensì sovrascritti.
		return states[first];
	}

	public int put(WorldState state) {

		try {
			this.mutex.lock();

			if (count == BUFFER_CAPACITY) {
				this.notFull.await();
			} else {

				this.last = (this.last + 1) % BUFFER_CAPACITY;
				this.count++;
				this.states[last] = state;
				this.notEmpty.signal();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			this.mutex.unlock();

		}
		return count;
	}

	/**
	 * Wake every thread that is waiting on a condition variable.
	 * (Used to shutdown the application.)
	 */
	public void wakeEveryOne(){
		this.mutex.lock();
		this.notFull.signalAll();
		this.notEmpty.signalAll();
		this.mutex.unlock();
	}
	
}
