package ass6.es2;

/**
 * 
 * Initial configuration example.
 * 
 * @author acco
 *
 */
public class FlyingWorldState extends WorldState{

	public FlyingWorldState(int generation) {
		super(generation);
	
		int x = World.ROWS/2 - 3;
		int y = World.COLUMNS/2 - 2;
		
		
		this.alive(x+4, y+3);
		this.alive(x+2, y+2);
		this.alive(x+2, y+1);
		this.alive(x+4, y+0);		
		
		this.setLivingCells(4);
		
	}

}
