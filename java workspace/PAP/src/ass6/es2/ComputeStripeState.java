package ass6.es2;

import java.util.concurrent.Callable;

/**
 * Logical task. It computes a portion of the next state matrix and returns the number
 * of living cells.
 * @author acco
 *
 */
public class ComputeStripeState implements Callable<Integer> {

	private int endRow;
	private int startRow;
	private WorldState currentState;
	private WorldState nextState;

	public ComputeStripeState(WorldState currentState, WorldState nextState, int startRow, int endRow) {
		this.currentState = currentState;
		this.nextState = nextState;
		this.startRow = startRow;
		this.endRow = endRow;
	}

	@Override
	public Integer call() throws Exception {
		
		int livingCells = 0;
		
		for(int i=startRow; i < endRow; i++){
			for(int j = 0; j < World.COLUMNS; j++){
				CellState cs = this.currentState.evolve(i, j);
				if(cs == CellState.ALIVE){
					this.nextState.alive(i, j);
					livingCells++;
				} else {
					this.nextState.dead(i, j);
				}
			}
		}
		
		return livingCells;
	}

}
