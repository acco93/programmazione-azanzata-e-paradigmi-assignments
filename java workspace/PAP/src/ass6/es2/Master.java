package ass6.es2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Thread that fills up the world state buffer using a
 * master-workers approach.
 * 
 * @author acco
 *
 */
public class Master extends Thread {

	
	private static final int N_TASKS = 30;

	private World world;

	private List<Future<Integer>>  tasksList;
	
	private ExecutorService exec;
	
	// listeners to notify
	private Set<BufferListener> bufferListeners;
	private Set<StateListener> stateListeners;

	// it is a field in order to show the world state when the
	// application starts and no computation has been done
	private WorldState initState;

	private SharedAppInfo sai;

	public Master(World world, SharedAppInfo sai) {
		
		this.world = world;
		this.sai = sai;
		
		tasksList = new ArrayList<>();
		int nThreads = Runtime.getRuntime().availableProcessors() + 1;
		exec = Executors.newFixedThreadPool(nThreads);
		this.bufferListeners = new HashSet<>();
		this.stateListeners = new HashSet<>();

		// 0 = generation
	
		 initState = new RandomWorldState(0);
		// initState = new FlyingWorldState(0);
		// initState = new GliderWorldState(0);
		this.world.put(initState);
	}

	@Override
	public void run() {

		int delta = World.ROWS / N_TASKS;
		
		int generation = 1;
		
		WorldState currentState = initState;
		WorldState nextState;
		
		while (!this.sai.isStopped()) {

			int currentRow = 0;
			
			nextState = new WorldState(generation);

			// split the state computation in stripes (data decomposition)
			for (int i = 0; i < N_TASKS-1; i++) {
				Future<Integer> f = this.exec.submit(new ComputeStripeState(currentState, nextState, currentRow, currentRow+delta));
				tasksList.add(f);
				currentRow += delta;
			}

			Future<Integer> f = this.exec.submit(new ComputeStripeState(currentState, nextState, currentRow, World.ROWS));
			tasksList.add(f);
			
			int livingCellsSum = 0;
			
			// then wait for them
			for (Future<Integer> task: tasksList) {
				try {
					livingCellsSum+=task.get();
				} catch (InterruptedException | ExecutionException e ) {
					e.printStackTrace();
				} catch (CancellationException c){
					
				}
			}
			
			nextState.setLivingCells(livingCellsSum);
			
			int nElem = this.world.put(nextState);
			
			this.notifyBufferListeners(nElem);
			
			
			currentState = nextState;
			//System.out.println(nextState);
			tasksList.clear();
			
			generation++;
		}
		
		this.exec.shutdown();
		
		this.notifyStateListeners(ProgramState.STOPPED);
	}

	private void notifyBufferListeners(int nElem) {
		
		for(BufferListener listener:this.bufferListeners){
			listener.bufferNotification(nElem);
		}
		
	}

	private void notifyStateListeners(ProgramState ps) {
		
		for(StateListener listener:this.stateListeners){
			listener.stateNotification(ps);
		}
		
	}

	
	public void addBufferListener(BufferListener listener) {
		this.bufferListeners.add(listener);
	}
	
	public void addStateListener(StateListener listener){
		this.stateListeners.add(listener);
	}
	
	
	
}
