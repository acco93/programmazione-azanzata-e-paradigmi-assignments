package ass6.es2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

/**
 * 
 * Form implementation.
 * 
 * @author acco
 *
 */
public class FormView extends JFrame implements View {

	private static final long serialVersionUID = 1L;
	private static final int FRAME_WIDTH = 1000;
	private static final int FRAME_HEGHT = 600;

	
	private JButton startButton;
	private JButton stopButton;
	private JLabel bufferLabel;
	private JLabel stateLabel;
	private DrawPanel drawPanel;
	private JLabel seedsInfo;
	private JButton pauseResumeButton;


	public FormView(Controller controller){
				
		this.setTitle("Seeds");
		this.setSize(FRAME_WIDTH, FRAME_HEGHT);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		/**
		 * Global panel
		 */
		JPanel globalPanel = new JPanel();
		globalPanel.setBorder(new EmptyBorder(10,10,10,10));
		this.add(globalPanel);
		globalPanel.setLayout(new BorderLayout());		
		
		/**
		 * Top panel
		 */
		JPanel topPanel = new JPanel();
		GridLayout topLayout = new GridLayout(1,3);
		topLayout.setHgap(5);
		topLayout.setVgap(5);
		topPanel.setLayout(topLayout);
		
		/**
		 * Start/restart button
		 */
		startButton = new JButton("Start");
		startButton.addActionListener((ActionEvent e)->{
			SwingUtilities.invokeLater(()->{
				startButton.setEnabled(false);
				pauseResumeButton.setEnabled(true);
				stopButton.setEnabled(true);	
			});
			controller.start();
		});
		
		/**
		 * Stop button
		 */
		stopButton = new JButton("Stop");
		stopButton.setEnabled(false);
		stopButton.addActionListener((ActionEvent e)->{
			SwingUtilities.invokeLater(()->{
				pauseResumeButton.setEnabled(false);
				stopButton.setEnabled(false);
				startButton.setEnabled(true);
				startButton.setText("Restart");
				pauseResumeButton.setText("Pause");
			});
			controller.stop();
		});
		
		/**
		 * Pause/resume rendering button
		 */
		pauseResumeButton = new JButton("Pause");
		pauseResumeButton.setEnabled(false);
		pauseResumeButton.addActionListener((ActionEvent e)->{
			SwingUtilities.invokeLater(()->{
				if(pauseResumeButton.getText().equals("Pause")){
					pauseResumeButton.setText("Resume");
				} else {
					pauseResumeButton.setText("Pause");
				}
				
			});
			controller.pauseResume();
		});
		
		/**
		 * Seeds info label
		 */
		seedsInfo = new JLabel("Processing info", SwingUtilities.CENTER);
		
		topPanel.add(startButton );
		topPanel.add(seedsInfo);
		topPanel.add(stopButton);
		topPanel.add(pauseResumeButton);
		topPanel.setBorder(new EmptyBorder(0,0,10,0));
		globalPanel.add(topPanel, BorderLayout.NORTH);
		
		/**
		 * Bottom panel
		 */
		JPanel bottomPanel = new JPanel();
		GridLayout bottomLayout = new GridLayout(1,2);
		bottomLayout.setHgap(5);
		bottomLayout.setVgap(5);
		bottomPanel.setLayout(bottomLayout);
		
		/**
		 * Buffer label
		 */
		bufferLabel = new JLabel("Sleeping ...", SwingConstants.CENTER);
		bufferLabel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		bufferLabel.setOpaque(true);
		bufferLabel.setBackground(new Color(255,200,0));
		
		/**
		 * State label
		 */
		stateLabel = new JLabel("Sleeping ...", SwingConstants.CENTER);
		stateLabel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		stateLabel.setOpaque(true);
		stateLabel.setBackground(Color.yellow);
		
		bottomPanel.add(bufferLabel);
		bottomPanel.add(stateLabel);
		bottomPanel.setBorder(new EmptyBorder(10,0,0,0));
		globalPanel.add(bottomPanel, BorderLayout.SOUTH);
		
		
		/**
		 * Draw panel
		 */
		drawPanel = new DrawPanel(this);
		
		globalPanel.add(drawPanel, BorderLayout.CENTER);
		

		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	@Override
	public void draw(WorldState state) {
		SwingUtilities.invokeLater(()->{
			this.drawPanel.setState(state);
			this.drawPanel.repaint();
			//this.graphPanel.addPointAndRepaint(new Point(state.getGeneration(), state.getLivingCells()));
		});
	}



	@Override
	public void changeBufferLabel(String text, Color color) {
		SwingUtilities.invokeLater(()->{
			this.bufferLabel.setText(text);	
			this.bufferLabel.setBackground(color);
		});
	}

	@Override
	public void changeStateLabel(String text, Color color) {
		SwingUtilities.invokeLater(()->{
			this.stateLabel.setText(text);
			this.stateLabel.setBackground(color);
		});
	}

	
	public void changeProcessingInfo(String text){
		SwingUtilities.invokeLater(()->{
			this.seedsInfo.setText(text);
		});
	}
}
