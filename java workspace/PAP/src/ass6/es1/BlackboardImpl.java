package ass6.es1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class BlackboardImpl implements Blackboard {

	private static final int FIRST_ELEMENT = 0;
	private HashMap<String, List<Msg>> data;

	public BlackboardImpl() {
		/*
		 * Hash map: tag -> [msg1, msg2, msg3, ...]
		 */
		this.data = new HashMap<String, List<Msg>>();
	}

	@Override
	public synchronized void post(String tag, Msg msg) {
	
		/*
		 * Retrieves the messages list, it could be:
		 * 	1 - with at least an item
		 * 	2 - instantiated but empty
		 *  3 - not even instantiated (null)
		 */
		Optional<List<Msg>> optList = Optional.ofNullable(this.data.get(tag));

		
		if (optList.isPresent()) {
			/*
			 * 1 & 2
			 */
			optList.get().add(msg);
		} else {
			/*
			 * 3
			 */
			LinkedList<Msg> newList = new LinkedList<Msg>();
			newList.add(msg);
			this.data.put(tag, newList);
		}

		this.notifyAll();

	}

	@Override
	public synchronized Msg take(String tag) {

		while (!this.data.containsKey(tag) || this.data.get(tag).isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		Msg msg = this.data.get(tag).remove(FIRST_ELEMENT);

		return msg;
	}

	@Override
	public synchronized Optional<Msg> takeIfPresent(String tag) {

		Optional<Msg> value = Optional.empty();

		if (this.data.containsKey(tag) && !this.data.get(tag).isEmpty()) {
			value = Optional.of(this.data.get(tag).remove(FIRST_ELEMENT));
		}

		return value;

	}

	@Override
	public synchronized Msg read(String tag) {
		while (!this.data.containsKey(tag) || this.data.get(tag).isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		Msg msg = this.data.get(tag).get(FIRST_ELEMENT);

		return msg;
	}

	@Override
	public synchronized Optional<Msg> readIfPresent(String tag) {
		Optional<Msg> value = Optional.empty();

		if (this.data.containsKey(tag) && !this.data.get(tag).isEmpty()) {
			value = Optional.of(this.data.get(tag).get(FIRST_ELEMENT));
		}

		return value;
	}

}
