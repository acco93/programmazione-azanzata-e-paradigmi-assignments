package lab07.actors.hello;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class Main {
  public static void main(String[] args) {
    ActorSystem system = ActorSystem.create("MySystem");
    /**
     * Il riferimento ad un attore è rappresentato da ActorRef e mi viene ritornato quando creo
     * l'attore. Props serve per gestire la configurazione del sistema. In questo caso la uso
     * da factory, c'è la possibilità anche di passare parametri al costruttore dell'attore.
     */
    ActorRef act = system.actorOf(Props.create(HappyActor.class));
    /**
     * messaggio + mittente (riferimento a cui rispondere)
     */
    act.tell(new HelloMsg("World"), ActorRef.noSender());
  }
}

/*
 * Gli oggetti che usiamo per rappresentare i messaggi è bene che siano immutabili & se possiede il riferimento
 * ad altri oggetti è bene che questi siano copie! Questo perchè gli attori non devono condividere memoria.
 * 
 * 
 */