package ass3.es1;

public class TextUtilTest {

	public static void main(String[] args) {
		
		String text = "Nel mezzo del cammin di nostra vita mi ritrovai per una selva oscura ché la diritta via era smarrita. Ahi quanto a dir qual era è cosa dura esta selva selvaggia e aspra e forte che nel pensier rinova la paura!";
		System.out.println(text);
		System.out.println("Words length: "+TextUtils.getWordsLength(text));
		System.out.println("Longest word: "+TextUtils.getWordWithMaxLen(text));
		System.out.println("Word (era) frequence: "+TextUtils.getWordFreq(text, "era"));
		System.out.println("Words positions: "+TextUtils.getWordsPos(text));
	}

}
