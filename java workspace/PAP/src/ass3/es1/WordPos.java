package ass3.es1;

import java.util.List;

public interface WordPos {

	String getWord();
	
	List<Integer> getPos();
}
