package ass3.es1;

public class Pair <T,U> {
	private T firstField;
	private U secondField;
	
	Pair (T firstField, U secondField){
		this.firstField = firstField;
		this.secondField = secondField;
	}
	
	public T first () { 
		return this.firstField;
	}
	
	public U second () {
		return this.secondField;
	}
	
}
