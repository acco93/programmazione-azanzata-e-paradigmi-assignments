package ass3.es2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class PointCloudTest {

	

	public static void main(String[] args) {
		
		Random r = new Random();
		
		List<P2d> init = new ArrayList<>();
		IntStream.range(0, 500).forEach(i -> {
			double x = Frame.XMIN + (Frame.XMAX - Frame.XMIN) * r.nextDouble();
			double y = Frame.YMIN + (Frame.YMAX - Frame.YMIN) * r.nextDouble();
			init.add(new P2d(x,y));
		});
		
		PointCloud pc = new MyPointCloud(init);
		System.out.println("Points:");
		System.out.println(pc.toString());
		System.out.println("\nPoints (moved of (1,1)):");
		pc.move(new V2d(1.0,1.0));
		System.out.println(pc.toString());
		System.out.println("\nPoints in region (0,0) - (2,2):");
		System.out.println(pc.getPointsInRegion(new Region(new P2d(0,0),new P2d(2,2))));
		System.out.println("\nNearest point (10,10):");
		System.out.println(pc.nearestPoint(new P2d(10,10)));
		
		new Frame(pc);
		
	}
	
}
