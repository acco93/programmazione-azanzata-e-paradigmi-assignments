package lab08;

import java.util.Random;

import rx.Observable; 
import rx.Subscriber;
import rx.observables.ConnectableObservable;

public class TestAsyncObsHot {

	public static void main(String[] args) throws Exception {
		
		// creating an asynchronous observable
	
		System.out.println("[MAIN] Creating the hot observable.");
		
		Observable<Integer> stream = Observable.create((Subscriber<? super Integer> subscriber) -> {
			new Thread(() -> {
				Random gen = new Random(System.currentTimeMillis());
				int i = 0;
				while (true) {
					try {
						int value = gen.nextInt();
						System.out.println("[GEN] "+i+" - value: "+value);
						subscriber.onNext(value);
						Thread.sleep(1000 + gen.nextInt() % 1000);
						i++;
					} catch (Exception ex){}
				}
			}).start();
		});	
		
		/*
		 * Creiamo un observable che genera elementi a prescindere da chi lo sta osservando.
		 * 
		 * Il generatore non esiste perchè io lo sto guardando, esiste sempre e ogni tanto io lo guardo.
		 * 
		 */
		ConnectableObservable<Integer> cos = stream.publish();
		cos.connect();
		
		Thread.sleep(5000);
		
		System.out.println("[MAIN] Subscribing.");
		
		/*
		 * quando faccio la subscribe => crea un nuovo thread => i ritorna 0
		 */
		stream.subscribe((Integer v) -> {
			System.out.println("[SUB] value observed: "+v);
			// while (true){}
		});
		
		// doing some job 
		
		while (true) {
			System.out.print(".");
			try {
				Thread.sleep(100);
			} catch (Exception ex){}			
		}
		
	}
}
