package lab08;

import rx.Observable;

public class HelloRx {

	public static void main(String[] args){
				
		System.out.println("Simple subscription to a sync source...");

		// synch data source
		
		String[] words = { "Hello", " ", "World", "!\n" }; 
		
		// simple subscription 
		
		Observable
			.from(words)
			.subscribe((String s) -> {
				/*
				 * Chi esegue questa lambda exp?
				 * Quello del main, se metto while(true) si blocca tutttto!
				 * => quando lavoriamo con sync data source il flusso di controllo che manda in esecuz
				 * la lambda exp è quello che fa la subscribe.
				 */
				System.out.print(s);
				// while(true);
			});

		// full subscription: onNext(), onError(), onCompleted()

		/*
		 * subscriber in grado di gestire più eventi: successo, errore e completamento.
		 */
		System.out.println("Full subscription...");
		
		Observable
			.from(words)
			.subscribe(
				/*
				 * Nuovo evento.
				 */
				(String s) -> {
					System.out.println("> " + s);
					try { 
						Thread.sleep(1000); 
					} catch (Exception ex){}
				},
				/*
				 * Errore.
				 */
	            (Throwable t) -> {
	                System.out.println("error  " + t);
	            },
	            /*
	             * Completamento.
	             */
	            () -> {
	                System.out.println("completed");
	            }
	        );

        System.out.println("here.");

	}
}
