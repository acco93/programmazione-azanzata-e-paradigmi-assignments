package ass8.es1;

import pap.ass08.pos.P2d;

/**
 * 
 * A pair of heartbeat value and position measured at the same instant.
 * 
 * @author acco
 *
 */
public class Measurment {

	private final int heartbeatValue;
	private final P2d position;

	public Measurment(int heartbeatValue, P2d position) {
		this.heartbeatValue = heartbeatValue;
		this.position = position;
	}

	public int getHeartbeatValue() {
		return heartbeatValue;
	}

	public P2d getPosition() {
		return position;
	}

}
