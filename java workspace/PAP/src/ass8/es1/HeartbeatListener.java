package ass8.es1;

/**
 * 
 * Methods implemented by an heartbeat listener.
 * 
 * @author acco
 *
 */
public interface HeartbeatListener {

	/**
	 * Notify the current heartbeat value.
	 * 
	 * @param value
	 */
	void notifyCurrentHBValue(int value);

	/**
	 * Notify the average heartbeat value up to now.
	 * 
	 * @param value
	 */
	void notifyAvgHBValue(int value);

	/**
	 * Trigger the alarm if necessary.
	 */
	void setHBAlarm();

	/**
	 * Shutdown the alarm.
	 */
	void unsetHBAlarm();

}
