package ass8.es1;

import pap.ass08.pos.P2d;

/**
 * 
 * Manages actions between model and view.
 * 
 * @author acco
 *
 */
public class Controller implements Listener {

	private View view;
	private Model model;

	public Controller() {
		this.view = new FormView(this);
		this.model = new Model();
		this.model.addListener(this);
	}

	/*
	 * Methods inherited from HeartbeatListener. Data flow: Model -> View
	 */

	@Override
	public void notifyCurrentHBValue(int value) {
		this.view.setCurrentHBValue(value);
	}

	@Override
	public void notifyAvgHBValue(int value) {
		this.view.setAvgHBValue(value);
	}

	@Override
	public void setHBAlarm() {
		this.view.setHBAlartm();
	}

	@Override
	public void unsetHBAlarm() {
		this.view.unsetHBAlarm();
	}

	/*
	 * Methods invoked by the view. Data flow: View -> Model
	 */

	/**
	 * Change the alarm threshold.
	 * 
	 * @param value
	 *            new threshold
	 */
	public void setHBThreshold(int value) {
		this.model.setHBAlarmThreshold(value);
		this.model.resetAlarms();
		this.unsetHBAlarm();
	}

	/**
	 * Change the time after which the alarm is fired.
	 * 
	 * @param time
	 *            new time
	 */
	public void setHBAlarmTime(int time) {
		this.model.setHBAlarmTime(time);
		this.model.resetAlarms();
		this.unsetHBAlarm();
	}

	/**
	 * Start/Resume monitoring.
	 */
	public void start() {
		this.model.startMonitoring();

	}

	/**
	 * Pause monitoring.
	 */
	public void stop() {
		this.model.stopMonitorting();
		this.model.resetAlarms();
	}

	/*
	 * Methods inherited from PositionListener.
	 */

	@Override
	public void notifyCurrentPosition(P2d value) {
		/*
		 * Map [-400,400][-300,300] in [0,800][0,600]
		 */
		double x = value.getX() + FormView.WIDTH / 2;
		double y = value.getY() + FormView.HEIGHT / 2;
		this.view.setCurrentPosition(new P2d(x, y));

	}

	@Override
	public void notifyCurrentSpeed(double speed) {
		this.view.notifyCurrentSpeed(speed);

	}

	@Override
	public void notifyAvgSpeed(double avgSpeed) {
		this.view.notifyAvgSpeed(avgSpeed);

	}

	@Override
	public void setPositionWithMaxHbValue(int hbValue, P2d posValue) {
		double x = posValue.getX() + FormView.WIDTH / 2;
		double y = posValue.getY() + FormView.HEIGHT / 2;
		this.view.setPositionWithMaxHbValue(hbValue, new P2d(x, y));
	}

}
