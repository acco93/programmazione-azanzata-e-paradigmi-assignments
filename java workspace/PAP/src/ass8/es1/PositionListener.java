package ass8.es1;

import pap.ass08.pos.P2d;

/**
 * Methods implemented by someone that want to listen for position related
 * information.
 * 
 * @author acco
 *
 */
public interface PositionListener {

	/**
	 * Notify the current position as a 2d point.
	 * 
	 * @param value
	 */
	void notifyCurrentPosition(P2d value);

	/**
	 * Notify the instantaneous velocity.
	 * 
	 * @param speed
	 */
	void notifyCurrentSpeed(double speed);

	/**
	 * Notify the average speed.
	 * 
	 * @param avgSpeed
	 */
	void notifyAvgSpeed(double avgSpeed);

}
