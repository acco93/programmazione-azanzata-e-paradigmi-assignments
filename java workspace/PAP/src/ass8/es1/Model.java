package ass8.es1;

import java.util.HashSet;
import java.util.Optional;

import pap.ass08.pos.P2d;
import rx.Observable;

/**
 * 
 * Model implementation.
 * 
 * @author acco
 *
 */
public class Model {

	/*
	 * Sampling rate.
	 */
	private static final int PERIOD = 100;
	/*
	 * HB support variables.
	 */
	private int hbSum;
	private int hbMeasurements;
	private int currentMaxHb;

	private HashSet<Listener> listeners;

	/*
	 * Alarm variables.
	 */
	private int alarmThreshold;
	private int alarmTimeout;
	private int alarmTime;

	/*
	 * Streams.
	 */
	private HeartbeatStream hs;
	private ResumablePosStream ps;

	/*
	 * Position related support variables.
	 */
	private Optional<P2d> lastPoint;
	private int positionMeasurements;
	private int speedSum;

	public Model() {

		this.listeners = new HashSet<>();

		/*
		 * Initial setup.
		 */
		this.hbSum = 0;
		this.hbMeasurements = 0;
		this.alarmThreshold = 90;
		this.lastPoint = Optional.empty();
		this.positionMeasurements = 0;
		this.speedSum = 0;
		/*
		 * Start the alarm after alartTime milliseconds.
		 */
		this.alarmTime = 2000;
		this.alarmTimeout = this.alarmTime;

		/*
		 * Define the hearbeat stream.
		 */
		hs = new HeartbeatStream(PERIOD);
		Observable<Integer> hsStream = hs.makeObservable();

		/*
		 * Define the position stream.
		 */
		ps = new ResumablePosStream(PERIOD);
		Observable<P2d> psStream = ps.makeObservable();

		/*
		 * Merge them in order to catch the measurement of both at the same
		 * time.
		 */
		Observable.zip(hsStream, psStream, (hbValue, posValue) -> {
			return new Measurment(hbValue, posValue);
		}).subscribe((measurment -> {

			int hbValue = measurment.getHeartbeatValue();
			P2d posValue = measurment.getPosition();

			this.processHeartbeatValue(hbValue);
			this.processPosValue(posValue);

			if (hbValue > this.currentMaxHb) {
				this.currentMaxHb = hbValue;
				for (Listener listener : listeners) {
					listener.setPositionWithMaxHbValue(hbValue, posValue);
				}
			}

		}));

	}

	/**
	 * Begin reading data from the streams.
	 */
	public void startMonitoring() {
		hs.resume();
		ps.resume();
	}

	/**
	 * Pause the reading (the sensors aren't stopped!)
	 */
	public void stopMonitorting() {
		hs.pause();
		ps.pause();
	}

	/**
	 * Process an incoming P2d data from the stream.
	 * 
	 * @param currentPoint
	 */
	private void processPosValue(P2d currentPoint) {

		double speed = 0.0;

		/*
		 * Compute the instantaneous speed.
		 */
		if (lastPoint.isPresent()) {
			speed = (P2d.distance(currentPoint, lastPoint.get()) / PERIOD) * 3600;
		}

		/*
		 * Save some support value to compute the average speed.
		 */
		positionMeasurements++;
		speedSum += speed;

		/*
		 * Notify the listeners.
		 */
		for (Listener listener : listeners) {
			listener.notifyCurrentPosition(currentPoint);
			listener.notifyCurrentSpeed(speed);
			listener.notifyAvgSpeed(speedSum / positionMeasurements);
		}

		/*
		 * Save the last point in order to compute the instantaneous speed.
		 */
		this.lastPoint = Optional.of(currentPoint);
	}

	/**
	 * Process an incoming hb value.
	 * 
	 * @param value
	 *            hb value
	 */
	private synchronized void processHeartbeatValue(Integer value) {

		/*
		 * Support measure to compute the average hb value.
		 */
		this.hbSum += value;
		this.hbMeasurements++;

		/*
		 * Alarm handling.
		 */
		if (value > this.alarmThreshold) {
			this.alarmTimeout -= PERIOD;
		} else {
			this.alarmTimeout = this.alarmTime;
		}

		/*
		 * Notify the listeners.
		 */
		for (Listener listener : listeners) {

			listener.notifyAvgHBValue(hbSum / hbMeasurements);
			listener.notifyCurrentHBValue(value);

			if (this.alarmTimeout <= 0) {
				this.alarmTimeout = 0;
				listener.setHBAlarm();
			} else {
				listener.unsetHBAlarm();
			}

		}
	}

	/**
	 * Add a listener that will be notified after every measurement.
	 * 
	 * @param listener
	 */
	public void addListener(Listener listener) {
		this.listeners.add(listener);

	}

	/**
	 * Edit the hb alarm threshold.
	 * 
	 * @param value
	 */
	public synchronized void setHBAlarmThreshold(int value) {
		this.alarmThreshold = value;

	}

	/**
	 * Edit the alarm time.
	 * 
	 * @param time
	 */
	public synchronized void setHBAlarmTime(int time) {
		this.alarmTime = time;
	}

	/**
	 * Unset the alarm when the pause button is pressed.
	 */
	public synchronized void resetAlarms() {
		this.alarmTimeout = this.alarmTime;
		for (Listener listener : listeners) {
			listener.unsetHBAlarm();
		}
	}

}
