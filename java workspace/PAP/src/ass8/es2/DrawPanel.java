package ass8.es2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import pap.ass08.pos.P2d;

/**
 * 
 * Display & update the map.
 * 
 * @author acco
 *
 */
public class DrawPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Color pointColor;
	private P2d currentPosition;
	/*
	 * The list of past positions.
	 */
	private List<P2d> history;
	private Optional<P2d> posWithMaxHb;

	public DrawPanel() {

		this.setBackground(new Color(82, 112, 72));
		pointColor = new Color(255, 232, 119);
		this.currentPosition = new P2d(-100, -100);
		this.history = new ArrayList<P2d>();
		this.posWithMaxHb = Optional.empty();
	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		g2.setColor(pointColor);

		/*
		 * Draw the line path till now.
		 */
		int x1 = 0;
		int y1 = 0;
		int x2 = (int) this.currentPosition.getX();
		int y2 = (int) this.currentPosition.getY();

		for (int i = 0; i < history.size() - 1; i++) {
			x1 = (int) history.get(i).getX();
			y1 = (int) history.get(i).getY();
			x2 = (int) history.get(i + 1).getX();
			y2 = (int) history.get(i + 1).getY();
			this.drawDashedLine(g2, x1 + 5, y1 + 5, x2 + 5, y2 + 5);
		}

		/*
		 * Draw the line that connects the last point with the current one.
		 */
		this.drawDashedLine(g2, x2, y2, (int) this.currentPosition.getX(), (int) this.currentPosition.getY());

		/*
		 * Draw the current position.
		 */
		g2.fillOval((int) this.currentPosition.getX(), (int) this.currentPosition.getY(), 10, 10);

		/*
		 * Draw the point with max heartbeat.
		 */
		this.posWithMaxHb.ifPresent(pos -> {
			g2.setColor(Color.red);
			g2.fillOval((int) pos.getX(), (int) pos.getY(), 10, 10);

		});

	}

	/*
	 * Taken from:
	 * http://stackoverflow.com/questions/21989082/drawing-dashed-line-in-java
	 */
	private void drawDashedLine(Graphics g, int x1, int y1, int x2, int y2) {

		// creates a copy of the Graphics instance
		Graphics2D g2d = (Graphics2D) g.create();

		// set the stroke of the copy, not the original
		Stroke dashed = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 9 }, 0);
		g2d.setStroke(dashed);
		g2d.drawLine(x1, y1, x2, y2);

		// gets rid of the copy
		g2d.dispose();
	}

	/**
	 * Update the current position and repaint the map.
	 * 
	 * @param p2d
	 */
	public void setCurrentPosition(P2d p2d) {
		this.currentPosition = p2d;
		this.history.add(p2d);
		SwingUtilities.invokeLater(() -> {
			this.repaint();
		});
	}

	/**
	 * Update the position with max hb.
	 * 
	 * @param posValue
	 */
	public void setPositionWithMaxHbValue(P2d posValue) {
		this.posWithMaxHb = Optional.of(posValue);

	}

}
