package ass8.es2;

/**
 * 
 * Wrap an alarm time update request.
 * 
 * @author acco
 *
 */
public class AlarmTimeMessage {

	private final int time;

	public AlarmTimeMessage(int time) {
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	
}
