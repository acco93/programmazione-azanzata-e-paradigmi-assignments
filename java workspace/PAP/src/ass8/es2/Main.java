package ass8.es2;

import akka.actor.ActorSystem;
import akka.actor.Props;

/**
 * Application entry point.
 * 
 * @author acco
 *
 */
public class Main {

	public static void main(String[] args) {

		ActorSystem system = ActorSystem.create("TrackBeat");

		system.actorOf(Props.create(DisplayerActor.class), Register.DISPLAYER_ACTOR);
		system.actorOf(Props.create(InspectorActor.class), Register.INSPECTOR_ACTOR);
		system.actorOf(Props.create(DetectorActor.class), Register.DETECTOR_ACTOR);

	}

}
