package ass8.es2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import rx.Observable;
import rx.Subscriber;

/**
 * 
 * Heart beat stream implementation as an independent control flow.
 * 
 * @author acco
 *
 */
public class HeartbeatStream {

	private int period;
	private HeartbeatSensor hbSensor;
	private Lock mutex;
	private boolean isPaused;
	private Condition cond;

	public HeartbeatStream(int period) {
		this.period = period;
		this.isPaused = true;
		this.hbSensor = new HeartbeatSensor();
		this.mutex = new ReentrantLock();
		this.cond = mutex.newCondition();
	}

	public Observable<Integer> makeObservable() {
		Observable<Integer> stream = Observable.create((Subscriber<? super Integer> subscriber) -> {
			new Thread(() -> {
				while (true) {
					try {

						mutex.lock();
						while (isPaused) {
							cond.await();
						}
						mutex.unlock();

						int value = this.hbSensor.getCurrentValue();
						subscriber.onNext(value);
						Thread.sleep(this.period);
					} catch (Exception ex) {
					}
				}
			}).start();
		});
		return stream;
	}

	public void pause() {
		mutex.lock();
		this.isPaused = true;
		mutex.unlock();
	}

	public void resume() {
		mutex.lock();
		this.isPaused = false;
		this.cond.signal();
		mutex.unlock();
	}

}
