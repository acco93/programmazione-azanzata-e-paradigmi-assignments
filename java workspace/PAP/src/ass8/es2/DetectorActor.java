package ass8.es2;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.japi.pf.ReceiveBuilder;

import pap.ass08.pos.P2d;
import rx.Observable;

/**
 * 
 * The detector actor wraps & handles the data streams produced by the sensors.
 * It sends the measurement to the inspector actor as soon as data are available.
 * 
 * @author acco
 *
 */
public class DetectorActor extends AbstractActor {

	/*
	 * Sampling rate.
	 */
	private static final int PERIOD = 100;
	
	public DetectorActor() {
		
		ActorSelection inspector = this.context().actorSelection("../"+Register.INSPECTOR_ACTOR);
		
		/*
		 * Define the hearbeat stream.
		 */
		HeartbeatStream hs = new HeartbeatStream(PERIOD);
		Observable<Integer> hsStream = hs.makeObservable();

		/*
		 * Define the position stream.
		 */
		ResumablePosStream ps = new ResumablePosStream(PERIOD);
		Observable<P2d> psStream = ps.makeObservable();
		
		/*
		 * Obtain a unique stream as a composition of hs & ps.
		 */
		Observable.zip(hsStream, psStream, (hbValue, posValue) -> {
			return new MeasurmentMessage(hbValue, posValue, PERIOD);
		}).subscribe((measurment -> {
			inspector.tell(measurment, self());
		}));
		
		/*
		 * Receive behavior.
		 */
		this.receive(ReceiveBuilder
				/*
				 * Pause the detection. NB: sensors are hot observable so they continue to produce data in spite of subscribers
				 */
				.match(PauseMessage.class, p ->{ hs.pause(); ps.pause();})
				/*
				 * Resume the detection.
				 */
				.match(ResumeMessage.class, r ->{ hs.resume(); ps.resume();})
				.matchAny(o -> {
			this.unhandled(o);
		}).build());
		
	}

}
