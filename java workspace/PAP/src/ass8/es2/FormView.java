package ass8.es2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import pap.ass08.pos.P2d;

/**
 * 
 * View implementation.
 * 
 * @author acco
 *
 */
public class FormView extends JFrame implements View {

	public static final int HEIGHT = 600;
	public static final int WIDTH = 800;
	private static final long serialVersionUID = 1L;

	private JLabel avgHB;
	private JLabel currentHB;
	private JLabel speed;
	private JButton startMenu;
	private JButton stopMenu;
	private DrawPanel drawPanel;
	private JLabel avgSpeed;
	private JLabel maxHbValue;

	public FormView(DisplayerActor displayer) {
		this.setSize(WIDTH, HEIGHT);
		
		/*
		 * The main panel holds all the components.
		 */
		JPanel mainPanel = new JPanel(new BorderLayout());
		
		/*
		 * Map panel.
		 */
		drawPanel = new DrawPanel();
		mainPanel.add(drawPanel, BorderLayout.CENTER);	
		
		/*
		 * Bottom panel & its labels.
		 */
		avgHB = new JLabel("Average HB: ---", SwingUtilities.CENTER);
		currentHB = new JLabel("Current HB: ---",SwingUtilities.CENTER);
		currentHB.setBackground(Color.red);
		maxHbValue = new JLabel("Max HB: ---",SwingUtilities.CENTER);
		JLabel statusLabel = new JLabel("Paused", SwingUtilities.CENTER);
		
		JPanel bottomPanel = new JPanel(new GridLayout(1,4));
		bottomPanel.add(currentHB);
		bottomPanel.add(avgHB);
		bottomPanel.add(maxHbValue);
		bottomPanel.add(statusLabel);
		
		mainPanel.add(bottomPanel, BorderLayout.SOUTH);
		
		this.add(mainPanel);
		
		JMenuBar menuBar = new JMenuBar();
		
		/*
		 * Start/resume button.
		 */
		startMenu = new JButton("Start monitoring");
		startMenu.addActionListener((e)->{
			displayer.start();
			startMenu.setEnabled(false);
			stopMenu.setEnabled(true);
			statusLabel.setText("Monitoring");
		});		
		menuBar.add(startMenu);
		
		/*
		 * Pause button.
		 */
		stopMenu = new JButton("Stop monitoring");
		stopMenu.addActionListener((e)->{
			displayer.stop();
			stopMenu.setEnabled(false);
			startMenu.setEnabled(true);
			statusLabel.setText("Paused");
		});
		stopMenu.setEnabled(false);
		menuBar.add(stopMenu);
		
		/*
		 * Configuration menu.
		 */
		JMenu configMenu = new JMenu("HB config");
		JMenuItem menuItemHBTh = new JMenuItem("Edit HB Alarm Threshold");
		menuItemHBTh.addActionListener((e)->{
			String input = JOptionPane.showInputDialog("Write the new alarm threshold:");
			int th;
			try{
				th = Integer.parseInt(input);
			} catch (NumberFormatException err){
				th = 90;
			}
			displayer.setHBThreshold(th);
		});
		JMenuItem menuItemAlarmTime = new JMenuItem("Edit Alarm Time");
		menuItemAlarmTime.addActionListener((e)->{
			String input = JOptionPane.showInputDialog("Write the new alarm time (milliseconds):");
			int time;
			try{
				time = Integer.parseInt(input);
			} catch (NumberFormatException err){
				time = 2000;
			}
			displayer.setHBAlarmTime(time);
		});
		configMenu.add(menuItemHBTh);
		configMenu.add(menuItemAlarmTime);
		menuBar.add(configMenu);
		
		/*
		 * Upper-Rightmost labels.
		 */
		menuBar.add(new JSeparator(SwingConstants.VERTICAL));
		speed = new JLabel("Speed: --- ");
		avgSpeed = new JLabel("Avg speed: --- ");
		menuBar.add(speed);
		menuBar.add(new JSeparator(SwingConstants.VERTICAL));
		menuBar.add(avgSpeed);
		
		this.setJMenuBar(menuBar);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);

		this.setVisible(true);
	}

	@Override
	public void setCurrentHBValue(int value) {
		SwingUtilities.invokeLater(() -> {
			this.currentHB.setText("Current HB: "+value);
		});
	}

	@Override
	public void setAvgHBValue(int value) {
		SwingUtilities.invokeLater(() -> {
			this.avgHB.setText("Average HB: "+value);
		});
	}

	@Override
	public void setHBAlartm() {
		SwingUtilities.invokeLater(()->{
			this.currentHB.setOpaque(true);
			this.currentHB.validate();
		});
		
	}

	@Override
	public void unsetHBAlarm() {
		SwingUtilities.invokeLater(()->{
			this.currentHB.setOpaque(false);
			this.currentHB.validate();
		});
		
	}

	@Override
	public void setCurrentPosition(P2d p2d) {
		drawPanel.setCurrentPosition(p2d);
		
	}

	@Override
	public void notifyCurrentSpeed(double speed) {
		SwingUtilities.invokeLater(()->{
			this.speed.setText("Speed: "+(int)speed +" m/h ");
		});
	}

	@Override
	public void notifyAvgSpeed(double avgSpeed) {
		SwingUtilities.invokeLater(()->{
			this.avgSpeed.setText("Avg speed: "+(int)avgSpeed +" m/h ");
		});
	}

	@Override
	public void setPositionWithMaxHbValue(int hbValue, P2d posValue) {
		SwingUtilities.invokeLater(()->{
			this.maxHbValue.setText("Max HB: "+ hbValue);
			this.drawPanel.setPositionWithMaxHbValue(posValue);
		});
	}

}
