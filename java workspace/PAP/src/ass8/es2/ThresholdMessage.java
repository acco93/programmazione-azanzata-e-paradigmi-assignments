package ass8.es2;

/**
 * Ask the inspector to edit the threshold alarm.
 * @author acco
 *
 */

public class ThresholdMessage {

	private final int threshold;

	public ThresholdMessage(int th) {
		this.threshold = th;
	}

	public int getThreshold() {
		return threshold;
	}

	
	
}
