package ass8.es2;

import java.util.Optional;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.japi.pf.ReceiveBuilder;
import pap.ass08.pos.P2d;

/**
 * 
 * The inspector actor analyzes the measurement received from the detector actor
 * and sends information to the displayer actor.
 * 
 * @author acco
 *
 */
public class InspectorActor extends AbstractActor {

	/*
	 * HB support variables.
	 */
	private int hbSum;
	private int hbMeasurements;
	private int currentMaxHb;

	/*
	 * Alarm variables.
	 */
	private int alarmThreshold;
	private int alarmTimeout;
	private int alarmTime;

	/*
	 * Position related support variables.
	 */
	private Optional<P2d> lastPoint;
	private int positionMeasurements;
	private int speedSum;

	private ActorSelection displayer;

	public InspectorActor() {

		displayer = this.context().actorSelection("../" + Register.DISPLAYER_ACTOR);

		/*
		 * Initial setup.
		 */
		this.hbSum = 0;
		this.hbMeasurements = 0;
		this.alarmThreshold = 90;
		this.lastPoint = Optional.empty();
		this.positionMeasurements = 0;
		this.speedSum = 0;
		/*
		 * Start the alarm after alartTime milliseconds.
		 */
		this.alarmTime = 2000;
		this.alarmTimeout = this.alarmTime;

		this.receive(ReceiveBuilder.match(MeasurmentMessage.class, m -> {
			this.processMeasurment(m);
		}).match(ThresholdMessage.class, m -> {
			this.alarmThreshold = m.getThreshold();
			this.resetAlarms();
		}).match(AlarmTimeMessage.class, m -> {
			this.alarmTime = m.getTime();
			this.resetAlarms();
		}).matchAny(o -> {
			this.unhandled(o);
		}).build());

	}

	/**
	 * Process a measurement.
	 * 
	 * @param measurment
	 */
	private void processMeasurment(MeasurmentMessage measurment) {

		int hbValue = measurment.getHeartbeatValue();
		P2d posValue = measurment.getPosition();
		int period = measurment.getPeriod();

		this.processHeartbeatValue(hbValue, period);
		this.processPosValue(posValue, period);

		if (hbValue > this.currentMaxHb) {
			this.currentMaxHb = hbValue;
			/*
			 * Notify the displayer of the new position with higher hb value.
			 */
			displayer.tell(new PositionWithMaxHbValueMessage(hbValue, posValue), self());
		}

	}

	/**
	 * Process an incoming P2d data from the stream.
	 * 
	 * @param currentPoint
	 */
	private void processPosValue(P2d currentPoint, int period) {

		double speed = 0.0;

		/*
		 * Compute the instantaneous speed.
		 */
		if (lastPoint.isPresent()) {
			speed = (P2d.distance(currentPoint, lastPoint.get()) / period) * 3600;
		}

		/*
		 * Save some support value to compute the average speed.
		 */
		positionMeasurements++;
		speedSum += speed;

		displayer.tell(new PositionMessage(currentPoint, speed, speedSum / positionMeasurements), self());

		/*
		 * Save the last point in order to compute the instantaneous speed.
		 */
		this.lastPoint = Optional.of(currentPoint);
	}

	/**
	 * Process an incoming hb value.
	 * 
	 * @param value
	 *            hb value
	 */
	private void processHeartbeatValue(Integer value, int period) {

		/*
		 * Support measure to compute the average hb value.
		 */
		this.hbSum += value;
		this.hbMeasurements++;

		/*
		 * Alarm handling.
		 */
		if (value > this.alarmThreshold) {
			this.alarmTimeout -= period;
		} else {
			this.alarmTimeout = this.alarmTime;
		}

		boolean triggerAlarm = false;

		if (this.alarmTimeout <= 0) {
			this.alarmTimeout = 0;
			triggerAlarm = true;
		}

		displayer.tell(new HeartMessage(value, hbSum / hbMeasurements, triggerAlarm), self());

	}

	/**
	 * Unset the alarm threshold/alarm time is changed.
	 */
	public void resetAlarms() {
		this.alarmTimeout = this.alarmTime;

	}

}
