package ass7.es2.oracleasmonitor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

/**
 * MVC view implementation.
 * @author acco
 *
 */
public class FormView extends JFrame implements View {

	private static final int HEIGHT = 400;
	private static final int WIDTH = 800;
	private static final long serialVersionUID = 1L;
	private JLabel stateLabel;
	private JButton startButton;
	private JButton pauseButton;
	private JList<Event> eventsList;
	private DefaultListModel<Event> events;

	public FormView(Controller controller){
		this.setTitle("Guess The Number");
		this.setSize(WIDTH, HEIGHT);
		
		/*
		 * Global panel
		 */
		JPanel globalPanel = new JPanel(new BorderLayout());
		this.add(globalPanel);
		
		/*
		 * State label
		 */
		stateLabel = new JLabel("Press start to begin", SwingUtilities.CENTER);
		stateLabel.setOpaque(true);
		stateLabel.setBackground(Color.yellow);
		globalPanel.add(stateLabel, BorderLayout.NORTH);
		
		/*
		 * Bottom layout + panel
		 */
		GridLayout bottomLayout = new GridLayout(1,2);
		JPanel bottomPanel = new JPanel(bottomLayout);
		
		/*
		 * Start/restart button
		 */
		startButton = new JButton("Start");
		startButton.addActionListener((e)->{
			controller.start();
			SwingUtilities.invokeLater(()->{
				this.stateLabel.setText("Running");
				stateLabel.setBackground(Color.green);
				this.notifyEvent(new Event("Game started!", EventSourceType.SYSTEM, EventType.DISPLAY));
				startButton.setEnabled(false);
				pauseButton.setEnabled(true);
			});
		});
		
		/*
		 * Pause/resume button 
		 */
		pauseButton = new JButton("Pause");
		pauseButton.setEnabled(false);
		pauseButton.addActionListener((e)->{
			SwingUtilities.invokeLater(()->{
				if(controller.getSharedInfo().isPaused()){
					this.notifyEvent(new Event("Game resumed!", EventSourceType.SYSTEM,EventType.DISPLAY));
					stateLabel.setText("Running");
					stateLabel.setBackground(Color.green);
					pauseButton.setText("Pause");
					controller.resume();
				} else {
					this.notifyEvent(new Event("Game paused!", EventSourceType.SYSTEM,EventType.DISPLAY));
					stateLabel.setText("Paused");
					stateLabel.setBackground(Color.yellow);
					pauseButton.setText("Resume");
					controller.pause();
				}	
			});
		});
		
		bottomPanel.add(startButton);
		bottomPanel.add(pauseButton);
		globalPanel.add(bottomPanel, BorderLayout.SOUTH);
		
		/*
		 * Events list (model)
		 */
		events = new DefaultListModel<>();
		events.addElement(new Event("App started", EventSourceType.SYSTEM, EventType.DISPLAY));

		/*
		 * Events list (swing component)
		 */
		eventsList = new JList<>(events);
		eventsList.setCellRenderer(new RowRenderer());
		eventsList.setBackground(Color.cyan);
	
		JScrollPane listScroller = new JScrollPane(eventsList);
		globalPanel.add(new JPanel().add(listScroller), BorderLayout.CENTER);
		
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@Override
	public void notifyEvent(Event event) {
		SwingUtilities.invokeLater(()->{
			/*
			 * add the element to the model list.
			 */
			this.events.addElement(event);
			
			/*
			 * make sure the last element is visible.
			 */
			int lastIndex = eventsList.getModel().getSize()-1;
			if (lastIndex >= 0) {
				eventsList.ensureIndexIsVisible(lastIndex);
			}
		});
		
	}

	@Override
	public void reset() {
		SwingUtilities.invokeLater(()->{
			this.startButton.setEnabled(true);
			this.startButton.setText("Restart");
			this.pauseButton.setEnabled(false);
			this.pauseButton.setText("Pause");
			this.stateLabel.setText("Game completed");
			this.stateLabel.setBackground(Color.MAGENTA);
		});
		
	}
	
}
