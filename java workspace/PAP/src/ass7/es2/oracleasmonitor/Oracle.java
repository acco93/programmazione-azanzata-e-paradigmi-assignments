package ass7.es2.oracleasmonitor;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * Oracle implementation as monitor.
 * 
 * @author acco
 *
 */
public class Oracle implements EventSource {

	public static int MAX_NUM = Integer.MAX_VALUE-1;
	public static int MIN_NUM = 0;
	private int currentTurn;
	private int magicNum;
	private int nPlayers;
	private int turnAttempts;
	private boolean gameCompleted;
	private boolean haveToWait;
	private ReentrantLock mutex;
	private Condition waitCondition;
	private Set<EventListener> listeners;

	public Oracle(int nPlayers, Set<EventListener> listeners) {
		
		this.listeners = listeners;
		
		Random r = new Random();
		this.magicNum = r.nextInt(((MAX_NUM - MIN_NUM) + 1) + MIN_NUM);
		
		this.log("Magic number: " + this.magicNum);

		this.gameCompleted = false;
		this.haveToWait = true;
		this.nPlayers = nPlayers;
		this.turnAttempts = 0;
		this.currentTurn = 0;

		/*
		 * Fair lock.
		 */
		this.mutex = new ReentrantLock(true);
		this.waitCondition = this.mutex.newCondition();

	}

	public Info guess(int value) {
		Info info = null;
		try {
			this.mutex.lock();

			/*
			 * Notify the players if they attempt to make a guess and the match is finished.
			 */
			if (this.gameCompleted && turnAttempts == 0) {
				return Info.GAME_COMPLETED;
			}

			/*
			 * Won! (There could be more winners).
			 */
			if (value == this.magicNum) {
				info = Info.WON;
				this.gameCompleted = true;
			} else {
				/*
				 * Prepare the hint
				 */
				if (value > this.magicNum) {
					info = Info.LOWER;
				} else {
					info = Info.HIGHER;
				}
			}

			/*
			 * This is first player for this turn.
			 */
			if (turnAttempts == 0) {
				this.haveToWait = true;
			}

			this.turnAttempts++;

			/*
			 * This is the last player for this turn.
			 */
			if (turnAttempts == nPlayers) {
				this.haveToWait = false;
				/*
				 * handle some turn variables.
				 */
				this.turnAttempts = 0;
				this.currentTurn++;
				if (!this.gameCompleted) {
					log("@ Turn " + currentTurn + " @");
				} else {
					/*
					 * send a reset event if the game is completed to the controller.
					 */
					this.sendEvent(new Event("reset",EventSourceType.ORACLE,EventType.FINISHED));
				}
				/*
				 * wake every waiting player.
				 */
				this.waitCondition.signalAll();
			}

			/*
			 * Wait for the last player. It will notify the others.
			 * This is how the turn is enforced.
			 */
			while (haveToWait) {
				this.waitCondition.await();
			}

			/*
			 * Notify the losers.
			 */
			if (this.gameCompleted && info != Info.WON) {
				info = Info.LOST;
			}

			

		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			
			this.mutex.unlock();
		}
		
		return info;		
		

	}

	private void log(String string) {
		//System.out.println("[oracle] :: " + string);
		
		this.sendEvent(new Event(string,EventSourceType.ORACLE, EventType.DISPLAY));
	}

	@Override
	public void sendEvent(Event event) {
		for(EventListener listener:listeners){
			listener.notifyEvent(event);
		}
	}
}
