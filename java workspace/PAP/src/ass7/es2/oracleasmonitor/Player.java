package ass7.es2.oracleasmonitor;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.Semaphore;



public class Player implements Runnable, EventSource {

	private String name;
	private Oracle oracle;
	private int number;
	private boolean gameCompleted;
	private boolean won;
	private Semaphore sem;
	private SharedInfo sd;
	private Set<EventListener> listeners;
	private int maxNum;
	private int minNum;

	public Player(String name, Oracle oracle, SharedInfo sd, Set<EventListener> listeners) {
		this.name = name;
		this.oracle = oracle;
		this.number = new Random().nextInt(((Oracle.MAX_NUM - Oracle.MIN_NUM) + 1) + Oracle.MIN_NUM);
		this.gameCompleted = false;
		this.won = false;
		this.sd = sd;
		this.sem = new Semaphore(0);
		this.listeners = listeners;
		this.minNum = Oracle.MIN_NUM;
		this.maxNum = Oracle.MAX_NUM;
	}

	@Override
	public void run() {

		while (!gameCompleted) {

			try {
				
				Thread.sleep(250);

			
				if (sd.isPaused()) {
					this.sem.acquire();
				}

				
				this.log("Sent number: " + this.number);
				Info info = this.oracle.guess(number);
	
				
				switch (info) {
				case WON:
					this.won = true;
					this.gameCompleted = true;
					break;
				case LOST:
					this.won = false;
					this.gameCompleted = true;
					break;
				case LOWER:
					this.maxNum = this.number-1;
					break;
				case HIGHER:
					this.minNum = this.number+1;
					break;
				case GAME_COMPLETED:
					this.gameCompleted = true;
					break;
				default:
					break;
				}
				
				this.number = this.randomInRange(this.minNum, this.maxNum);	

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		if (this.won) {
			log("Won!");
		} else {
			log("Sob...");
		}

	}

	public void resume() {
		this.sem.release();
	}

	private void log(String string) {
		// System.out.println("[" + name + "] :: " + string);
		this.sendEvent(new Event("[" + name + "] :: " + string, EventSourceType.PLAYER, EventType.DISPLAY));
	}

	private int randomInRange(int min, int max) {
		    int range = (max - min) + 1;     
		    return (int)(Math.random() * range) + min;
		 }
	
	@Override
	public void sendEvent(Event event) {
		for (EventListener listener : listeners) {
			listener.notifyEvent(event);
		}
	}

}
