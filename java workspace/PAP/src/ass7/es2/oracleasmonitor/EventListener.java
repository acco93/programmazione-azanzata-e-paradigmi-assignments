package ass7.es2.oracleasmonitor;

public interface EventListener {

	/**
	 * Handle events coming from the model.
	 * @param event
	 */
	void notifyEvent(Event event);
	
}
