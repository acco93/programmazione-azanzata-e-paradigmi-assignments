package ass7.es2.oracleasmonitor;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.time.format.DateTimeFormatter;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

/**
 * 
 * Custom JList row.
 * 
 * @author acco
 *
 */
public class RowRenderer extends JPanel implements ListCellRenderer<Event> {

	private static final long serialVersionUID = 1L;
	private JLabel info;
	private JLabel type;
	private JLabel time;

	public RowRenderer(){
		GridLayout layout = new GridLayout(1,3);
		
		layout.setVgap(5);	
		this.setLayout(layout);
		this.info = new JLabel("",SwingUtilities.LEFT);
		this.info.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.time = new JLabel("",SwingUtilities.CENTER);
		this.type = new JLabel("",SwingUtilities.CENTER);
		this.type.setOpaque(true);
		this.add(time);
		this.add(info);
		this.add(type);		
		this.setOpaque(false);
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Event> list, Event event, int index,
			boolean isSelected, boolean cellHasFocus) {
		
		
		this.time.setText(event.getTime().format(DateTimeFormatter.ofPattern("HH:mm:ss.SSS")).toString());
		this.info.setText(event.getInfo());
		this.type.setText(event.getSource().toString());

		
		switch(event.getSource()){
		case ORACLE:
			this.type.setBackground(Color.orange);
			break;
		case PLAYER:
			this.type.setBackground(Color.yellow);
			break;
		case SYSTEM:
			this.type.setBackground(Color.green);
			break;
		default:
			break;
		}
		
		if(isSelected){
			this.setBorder(BorderFactory.createRaisedBevelBorder());
		} else {
			this.setBorder(BorderFactory.createEmptyBorder());
			
		}
		
		
		return this;
	}

}
