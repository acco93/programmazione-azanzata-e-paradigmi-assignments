package ass7.es2.oracleasmonitor;

public interface EventSource {
	
	void sendEvent(Event event);
	
}
