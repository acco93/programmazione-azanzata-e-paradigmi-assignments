package ass7.es2.concurrent;

import java.util.Random;
import java.util.Set;

/**
 * 
 * Oracle implementation.
 * 
 * @author acco
 *
 */
public class Oracle extends Thread implements EventSource {

	public static int MAX_NUM = Integer.MAX_VALUE-1;
	public static int MIN_NUM = 0;

	private int magicNum;

	private Set<EventListener> listeners;
	private boolean gameCompleted;
	private OracleSharedData osd;
	private int currentTurn;

	public Oracle(Set<EventListener> listeners, OracleSharedData osd) {
		
		this.listeners = listeners;
		this.osd = osd;
		
		
		Random r = new Random();
		this.magicNum = r.nextInt(((MAX_NUM - MIN_NUM) + 1) + MIN_NUM);
		
		this.log("Magic number: " + this.magicNum);
		this.currentTurn = 0;
		this.gameCompleted = false;
	}

	
	
	@Override
	public void run() {
		
		while(!this.gameCompleted){
			log("@ Turn " + currentTurn + " @");
			currentTurn++;
			this.gameCompleted = osd.reply(magicNum);
			
		}
		this.sendEvent(new Event("reset",EventSourceType.ORACLE,EventType.FINISHED));
		
	}



	private void log(String string) {
		//System.out.println("[oracle] :: " + string);
		
		this.sendEvent(new Event(string,EventSourceType.ORACLE, EventType.DISPLAY));
	}

	@Override
	public void sendEvent(Event event) {
		for(EventListener listener:listeners){
			listener.notifyEvent(event);
		}
	}
}
