package ass7.es2.concurrent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * Data shared between players and oracle.
 * Quasi-monitor object.
 * 
 * @author acco
 *
 */
public class SharedData implements OracleSharedData, PlayerSharedData {

	/*
	 * Map<PlayerId, Pair<Guess, Reply>>
	 * 
	 * Players guess is saved into the map. When all players have made their guess
	 * the oracle is woken up to set the hints (Info) for each guess. 
	 * 
	 */
	private Map<Long, MutablePair<Integer, Info>> guessReply;
	private Lock lock;
	private Condition playersCondition;
	private CyclicBarrier barrier;
	private boolean repliesDone;
	private Condition oracleCondition;
	private boolean guessesDone;
	private boolean areThereWinners;

	public SharedData(int nPlayers) {
		this.guessReply = new HashMap<>();
		this.lock = new ReentrantLock(true);
		this.playersCondition = this.lock.newCondition();
		this.oracleCondition = this.lock.newCondition();
		this.repliesDone = false;
		this.guessesDone = false;
		this.areThereWinners = false;
		
		this.barrier = new CyclicBarrier(nPlayers, () -> {

			this.lock.lock();
			/*
			 * The barrier is broken then all players have made their guess.
			 */
			this.guessesDone = true;
			/*
			 * It is the oracle time to set the replies.
			 */
			this.repliesDone = false;
			/*
			 * Wake up the oracle
			 */
			this.oracleCondition.signal();
			
			this.lock.unlock();
		});
		
	}

	@Override
	public Info guess(int value) {

		/*
		 * Retrieve a unique thread identifier, used as key to fill the map.
		 */
		long playerId = Thread.currentThread().getId();

		/*
		 * Set the guess value into the pair (guess-reply) object.
		 */
		MutablePair<Integer, Info> attempt = new MutablePair<Integer, Info>(value);

		/*
		 * Put the pair avoiding race conditions.
		 */
		lock.lock();
		this.guessReply.put(playerId, attempt);
		lock.unlock();
		
		/*
		 * Wait till all players have made their guess.
		 * 
		 * NB: the barrier await doesn't release the lock!!
		 * So I can't hold it!
		 * This is the reason why it's not a real monitor.
		 * 
		 */
		try {
			this.barrier.await();
		} catch (InterruptedException | BrokenBarrierException e1) {
			
		}

		/*
		 * Catch the lock in order to wait on the condition variable.
		 */
		lock.lock();

		while (!this.repliesDone) {
			try {
				this.playersCondition.await();
			} catch (InterruptedException e) {

			}
		}
		
		lock.unlock();

		/*
		 * I've been woken up by the oracle thus I can get my reply.
		 */
		
		Info reply = this.guessReply.get(playerId).getSecond().get();

		return reply;

	}

	@Override
	public boolean reply(int magicNumber) {

		try {
			lock.lock();

			/*
			 * Wait the guesses.
			 */
			while (!this.guessesDone) {
				try {
					this.oracleCondition.await();
				} catch (InterruptedException e) {

				}
			}

			/*
			 * Provide the replies.
			 */
			for (Long playerId : this.guessReply.keySet()) {
				MutablePair<Integer, Info> mutablePair = this.guessReply.get(playerId);
				int value = mutablePair.getFirst().get();
				Info info;
				if (value == magicNumber) {
				
					this.areThereWinners = true;
					info = Info.WON;
				
				} else {
					
					if (value > magicNumber) {
						info = Info.LOWER;
					} else {
						info = Info.HIGHER;
					}

				}

				mutablePair.setSecond(info);

			}

			/*
			 * If there are winners I have to re-set the replies given before the first winner was found.
			 */
			if (areThereWinners) {

				for (Long playerId : this.guessReply.keySet()) {
					MutablePair<Integer, Info> mutablePair = this.guessReply.get(playerId);
					Info info = mutablePair.getSecond().get();

					if (info != Info.WON) {
						mutablePair.setSecond(Info.LOST);
					}

				}

			}

			/*
			 * Reset players variables / barrier.
			 */

			this.barrier.reset();
			this.guessesDone = false;
			this.repliesDone = true;
			
			/*
			 * Wake up the players.
			 */

			this.playersCondition.signalAll();

			return this.areThereWinners;

		} finally {

			lock.unlock();
		}

	}

}
