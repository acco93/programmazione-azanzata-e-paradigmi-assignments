package ass7.es2.concurrent;

/*
 * MVC view interface.
 */
public interface View {
	
	void notifyEvent(Event event);
	void reset();
}
