package ass7.es2.concurrent;

/**
 * 
 * Players view on shared data object. 
 * 
 * @author acco
 *
 */

public interface PlayerSharedData {

	Info guess(int value);
	
}
