package ass7.es2.concurrent;

public interface EventSource {
	
	void sendEvent(Event event);
	
}
