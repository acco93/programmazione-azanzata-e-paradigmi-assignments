package ass7.es1;


import java.util.concurrent.TimeUnit;

import akka.actor.ActorSystem;
import akka.actor.Props;
import scala.concurrent.duration.Duration;

public class Main {

	/*
	 * Oracle logic identifier.
	 */
	public static final String ORACLE = "oracle";

	public static void main(String[] args) throws InterruptedException {
		
		int nPlayers = 2;
		
		if(args.length > 0){
			try{
			nPlayers = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
			}
		}
		
		ActorSystem system = ActorSystem.create("GuessTheNumber");

		system.actorOf(Props.create(OracleActor.class), ORACLE);
		
		for(int i=0; i< nPlayers; i++){
			system.actorOf(Props.create(PlayerActor.class,"player"+i),"player"+i);
		}
		
		/*
		 * Actor arrived (maybe) too late.
		 */
		
		system.scheduler().scheduleOnce(Duration.create(OracleActor.REGISTRATION_TIMEOUT+1, TimeUnit.MILLISECONDS), ()->{
			system.actorOf(Props.create(PlayerActor.class,"laggard"),"laggard");
		}, system.dispatcher());
		
	}

}
