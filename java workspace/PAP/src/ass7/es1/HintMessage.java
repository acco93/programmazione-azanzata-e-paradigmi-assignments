package ass7.es1;

public class HintMessage {

	enum Hint { LOWER, HIGHER}

	private final Hint hint;
	
	public HintMessage(Hint hint){
		this.hint = hint;
	}
	
	public Hint get(){
		return this.hint;
	}

	public static HintMessage lower() {
		return new HintMessage(Hint.LOWER);
	}
	
	public static HintMessage higher() {
		return new HintMessage(Hint.HIGHER);
	}
}
