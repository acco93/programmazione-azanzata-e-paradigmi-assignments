package ass7.es1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.japi.pf.ReceiveBuilder;
import scala.PartialFunction;
import scala.concurrent.duration.Duration;
import scala.runtime.BoxedUnit;

public class OracleActor extends AbstractActor {

	/*
	 * The oracle waits for players REGISTRATION_TIMEOUT milliseconds then
	 * starts the game. The oracle doesn't know the number of players.
	 */
	public static final long REGISTRATION_TIMEOUT = 10000;
	/*
	 * Shutdown the actor system after GAME_TIMEOUT seconds.
	 */
	private static final int GAME_TIMEOUT = 60;
	/*
	 * Set containing the registered players.
	 */
	private Set<ActorRef> players;
	/*
	 * There could be more than a winner when more players send the right number
	 * in the same turn.
	 */
	private Set<ActorRef> winners;
	/*
	 * Game parameters.
	 */
	private int minNum;
	private int maxNum;
	private int magicNum;
	private int currentTurn;
	/*
	 * Store the replies and send them when all the players have sent the guess.
	 * Used to check for double guess attempts.
	 */
	private Map<ActorRef, HintMessage> playerReply;
	
	private Cancellable gameTimeoutTask;


	public OracleActor(){
		
		/*
		 * Setting up game parameters.
		 */
		
		this.minNum = 0;
		this.maxNum = Integer.MAX_VALUE-1;
		this.magicNum = new Random().nextInt(((maxNum - minNum) + 1) + minNum);
		this.currentTurn = 0;
		this.playerReply = new HashMap<>();
		
		this.log("Magic number: "+this.magicNum);
		
		this.players = new HashSet<ActorRef>();
		this.winners = new HashSet<ActorRef>();
		
		this.log("Waiting for players! ["+REGISTRATION_TIMEOUT/1000+" seconds]");
				
		/*
		 * Start waiting for players registration (for a fixed time: REGISTRATION_TIMEOUT milliseconds).
		 */
		this.receive(OracleBehavior.initialState(this));
		
		/*
		 * Schedule the game state beginning.
		 */
		this.context().system().scheduler().scheduleOnce(Duration.create(REGISTRATION_TIMEOUT, TimeUnit.MILLISECONDS),()->{
			if(players.size() == 0){
				/*
				 * No players.
				 */
				log("No players... Bye!");
				context().become(OracleBehavior.endState(OracleActor.this));
			} else {
				/*
				 * Switch to the game state.
				 */
				context().become(OracleBehavior.gameState(OracleActor.this));
				
				log("Game started!");
				log("@ Turn "+currentTurn+" @");
				
				/*
				 * Send the players the game parameters.
				 */
				for(ActorRef player:players){
					player.tell(new SetupMessage(minNum, maxNum), self());
				}
			}
		}, this.context().dispatcher());
		
		/*
		 * Shutdown the system after Long.MAX_VALUE seconds.
		 * If something goes wrong during the match.
		 */
		
		gameTimeoutTask = this.context().system().scheduler().scheduleOnce(Duration.create(GAME_TIMEOUT, TimeUnit.SECONDS),()->{
			log("Game timeout reached. Shutting down!");
			context().system().shutdown();
			
		}, this.context().dispatcher());
		
	}

	/**
	 * Log method.
	 * @param string
	 */
	private void log(String string) {
		System.out.println("[oracle] :: " + string);
	}

	/**
	 * Send stored hint messages to players.
	 */
	private void sendReplies() {
		for(ActorRef player:players){
			HintMessage reply = this.playerReply.remove(player);
			player.tell(reply, player);
		}
		
	}
	
	/**
	 * 
	 * Oracle actor behaviors factory.
	 * 
	 * @author acco
	 *
	 */
	
	public static class OracleBehavior {

		/**
		 * Initial state handles the players registration.
		 * The oracle actor doesn't know the players number.
		 * @param oracle
		 * @return initial state
		 */
		public static PartialFunction<Object, BoxedUnit> initialState(OracleActor oracle) {
			return ReceiveBuilder
			/*
			 * Registration messages handler.
			 */
			.match(RegistrationMessage.class, r -> {
				oracle.log("Hello " + oracle.sender().path().name());
				oracle.players.add(oracle.sender());
			})
			/*
			 * Other messages..
			 */
			.matchAny(o -> {
				oracle.log("Unhandled message, please send a registration message.");
			}).build();
		}

		/**
		 * Game state handles incoming guess messages.
		 * @param oracle
		 * @return game state
		 */
		public static PartialFunction<Object, BoxedUnit> gameState(OracleActor oracle){
			return ReceiveBuilder
			/*
			 * Registration messages: unfortunately the match has already started :(
			 */
			.match(RegistrationMessage.class, r -> {
				oracle.log("You're late " + oracle.sender().path().name());
			})
			/*
			 * Guess messages handler.
			 */
			.match(GuessMessage.class, guess -> {
				/*
				 * Check if the player has already sent the guess for the current turn.
				 */
				if(oracle.playerReply.containsKey(oracle.sender())){
					/*
					 * Discard the message & (maybe) send a double guess error message.
					 */
					oracle.log("Double guess attempt from " + oracle.sender().path().name());
					//oracle.unhandled(guess);
				} else {
					/*
					 * Legitimate guess attempt.
					 */
					oracle.log("Guess received ("+guess.get()+") from " + oracle.sender().path().name());
					if(guess.get() == oracle.magicNum){
						/*
						 * Guessed! There could be other winners (in this turn).
						 */
						oracle.log("Winner: " + oracle.sender().path().name());
						oracle.winners.add(oracle.sender());
					
					} else {
						/*
						 * Ouch.. wrong guess. Prepare the hint.
						 */
						HintMessage reply;
						if(guess.get() > oracle.magicNum){
							reply = HintMessage.lower();
						} else {
							reply = HintMessage.higher();
						}
						/*
						 * Store the reply. All the messages will be sent as soon as the last
						 * player has made his guess.
						 */
						oracle.playerReply.put(oracle.sender(), reply);
					
					}
					
					/*
					 * When all players have made their guess..
					 */
					if(oracle.playerReply.size() == (oracle.players.size()-oracle.winners.size()) && oracle.winners.size()>0){
						/*
						 * 1) there is at least one winner => the match is finished.
						 */
						oracle.log("Game ended");
						/*
						 * Notify the winner(s).
						 */
						for(ActorRef winner:oracle.winners){
							winner.tell(InfoMessage.won(), oracle.self());
						}
						/*
						 * Notify the losers.
						 */
						oracle.players.removeAll(oracle.winners);
						for(ActorRef player:oracle.players){
							player.tell(InfoMessage.lost(), oracle.self());
						}
						/*
						 * Re-merge players and winners.
						 */
						oracle.players.addAll(oracle.winners);
						/*
						 * Empty the replies map to avoid the else if branch below.
						 */
						oracle.playerReply.clear();
						/*
						 * Switch to the end state and wait for end messages before shutting down the system.
						 */
						oracle.context().become(OracleBehavior.endState(oracle));
					} else if(oracle.playerReply.size() == oracle.players.size()){
						/*
						 * 2) no one has guessed the magic number. Ok send the replies!
						 */	
						oracle.sendReplies();
						/*
						 * Increment the turn.
						 */
						oracle.currentTurn++;
						oracle.log("@ Turn "+oracle.currentTurn+" @");
					}
					
				}
			}).matchAny(o -> {
				oracle.log("Unhandled message, please send a guess message.");
			}).build();
		}

		/**
		 * End state.
		 * @param oracle
		 * @return end state.
		 */
		public static PartialFunction<Object, BoxedUnit> endState(OracleActor oracle) {
			return ReceiveBuilder
			/*
			 * End message handler. 
			 */
			.match(EndMessage.class, msg-> oracle.players.contains(oracle.sender()),msg->{
				oracle.log("Goodbye "+oracle.sender().path().name());
				oracle.players.remove(oracle.sender());
				if(oracle.players.size()==0){
					oracle.log("Game succesflully completed! Shutting down the system.");
					oracle.gameTimeoutTask.cancel();
					oracle.context().system().shutdown();
				}
			})		
			.matchAny(m->{
				oracle.log("You're late " + oracle.sender().path().name()+" [game ended] @ ["+m.getClass()+"]");
				//oracle.unhandled(m);
			}).build();
		}

	}



}
