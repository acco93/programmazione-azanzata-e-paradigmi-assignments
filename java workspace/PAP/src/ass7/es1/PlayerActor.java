package ass7.es1;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.japi.pf.ReceiveBuilder;
import ass7.es1.HintMessage.Hint;
import ass7.es1.InfoMessage.Info;

public class PlayerActor extends AbstractActor {

	private int minNum;
	private int maxNum;
	private int number;
	private String name;
	private ActorSelection oracle;

	public PlayerActor(String name) {
		this.name = name;
		/*
		 * Get the oracle (local) reference.
		 */
		oracle = this.context().actorSelection("../" + Main.ORACLE);
		/*
		 * Lets register!
		 */
		log("Sending the registration message");
		oracle.tell(new RegistrationMessage(), this.self());

		this.receive(ReceiveBuilder
				/*
				 * Setup message handler.
				 */
				.match(SetupMessage.class, s -> {
					/*
					 * Choose a number according to the received setup.
					 */
					this.minNum = s.getMinNum();
					this.maxNum = s.getMaxNum();
					this.number = this.randomInRange(this.minNum, this.maxNum);
					/*
					 * Start guessing & switch to the guessing state (to ignore
					 * following setup messages).
					 */
					oracle.tell(new GuessMessage(this.number), self());
					this.context()
							.become(ReceiveBuilder
									/*
									 * Hint messages handler.
									 */
									.match(HintMessage.class, hint -> {

										if (hint.get() == Hint.HIGHER) {
											this.minNum = this.number + 1;
										} else {
											this.maxNum = this.number - 1;
										}

										this.number = this.randomInRange(this.minNum, this.maxNum);

										this.log("Sent number: " + this.number);

										this.oracle.tell(new GuessMessage(this.number), this.self());

									})
									/*
									 * Info messages handler.
									 */
									.match(InfoMessage.class, info -> {
										if (info.get() == Info.WON) {
											log("Won!");
										} else {
											log("Sob...");
										}
										log("Sending end message.");
										oracle.tell(new EndMessage(), this.self());
										getContext().stop(self());
									}).matchAny(o -> this.unhandled(o)).build());
				}).matchAny(o -> this.unhandled(o)).build());

	}

	private int randomInRange(int min, int max) {
		int range = (max - min) + 1;
		return (int) (Math.random() * range) + min;
	}

	/**
	 * Log function.
	 * 
	 * @param string
	 */
	private void log(String string) {
		System.out.println("\t[" + name + "] :: " + string);
	}

}
