package ass4.es2;

/**
 * Abstract implementation of IAnimation. It defines the draw method implementation delegating the 
 * concrete implementation of render, clear, transform and getDrawable to classes that extend it.
 * 
 * @author acco
 *
 */

public abstract class AbstractAnimation implements IAnimation {
	
	
	@Override
	public void draw() {
		
		this.clear();
		this.transform();
		this.render();
	
	}

	/**
	 * Draws the animated drawable to the screen.
	 */
	protected abstract void render();
	/**
	 * Removes the animated drawable from the screen.
	 */
	protected abstract void clear();
	/**
	 * Changes the drawable's properties.
	 */
	protected abstract void transform();
	/**
	 * Returns the drawable for which the animation exists.
	 * @return
	 */
	protected abstract Drawable getDrawable();
	
	
}
