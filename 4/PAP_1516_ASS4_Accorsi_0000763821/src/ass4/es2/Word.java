package ass4.es2;

import java.util.stream.IntStream;

import ass4.es2.TextLib.Color;

/**
 * A string shaped drawable.
 * @author acco
 *
 */
public class Word implements Drawable {

	private static final Color DEFAULT_COLOR = Color.WHITE;
	private String word;
	
	private int y;
	private int x;
	private Color color;

	private TextLib lib;

	Word(String word) {
		this.word = word;
		this.x = 0;
		this.y = 0;
		this.setColor(DEFAULT_COLOR);
		this.lib = TextLibFactory.getInstance();
	}

	@Override
	public Color getColor() {
		return this.color;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int getWidth() {
		return this.word.length();
	}

	@Override
	public int getHeight() {
		return 1;
	}

	@Override
	public void render() {
		lib.writeAt(this.getX(), this.getY(), this.word, this.getColor());
	}

	@Override
	public void clear() {
		IntStream.range(0, this.getWidth()).forEach(i -> lib.writeAt(this.getX()+i, this.getY(), " "));
	}

	
	public String getWord(){
		return this.word;
	}
}
