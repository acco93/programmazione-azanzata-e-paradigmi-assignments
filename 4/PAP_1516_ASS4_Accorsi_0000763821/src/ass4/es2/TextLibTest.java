package ass4.es2;

import java.util.Arrays;

public class TextLibTest {

	public static void main(String[] args) throws Exception {		
		TextLib lib = TextLibFactory.getInstance();
		lib.cls();
		
		// creo la viewport sul terminale
		DrawingEnvironment screen = new DrawingEnvironment(2,10,80,20);
		
		
		// per ogni parola passata creo un thread
		Arrays.asList(args).stream().forEach(word -> {
			screen.addDrawable(new Word(word));
		});
	
		screen.draw();
		
		// creo la viewport sul terminale
		DrawingEnvironment screenB = new DrawingEnvironment(82,10,80,20);
		Arrays.asList(args).stream().forEach(word -> {
			screenB.addDrawable(new Word(word));
		});
		screenB.draw();
		
		
		
		
	}

}
