package ass4.es2;

/**
 * The simplest animation. Just the real motionless drawable.
 * @author acco
 *
 */
public class StaticAnimation extends AbstractAnimation {
	

	private Drawable drawable;

	StaticAnimation(Drawable drawable){
		this.drawable = drawable;
	}
	
	@Override
	protected void render() {
		this.drawable.render();
	}

	@Override
	protected void clear() {
		this.drawable.clear();
	}

	@Override
	protected void transform() {
		
	}

	@Override
	protected Drawable getDrawable() {
		return this.drawable;
	}


}
