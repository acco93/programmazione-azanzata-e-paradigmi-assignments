package ass4.es2;

import ass4.es2.TextLib.Color;

/**
 * Defines some useful methods & properties getters/setters for a drawable
 * object.
 * 
 * @author acco
 *
 */
public interface Drawable {

	/**
	 * Draw the object.
	 */
	void render();

	/**
	 * Clear the object.
	 */
	void clear();

	/**
	 * Returns the object color.
	 * 
	 * @return color
	 */
	Color getColor();

	/**
	 * Sets the object color.
	 * 
	 * @param color
	 */
	void setColor(Color color);

	/**
	 * Returns the x coordinate.
	 * 
	 * @return x
	 */
	int getX();

	/**
	 * Set the x coordinate.
	 * 
	 * @param x
	 */
	void setX(int x);

	/**
	 * Returns the y coordinate.
	 * 
	 * @return y
	 */
	int getY();

	/**
	 * Set the y coordinate.
	 * 
	 * @param y
	 */
	void setY(int y);

	/**
	 * Returns the object width.
	 * 
	 * @param width
	 */
	int getWidth();

	/**
	 * Returns the object height.
	 * 
	 * @param height
	 */
	int getHeight();
}
