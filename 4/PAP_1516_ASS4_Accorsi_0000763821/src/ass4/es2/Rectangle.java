package ass4.es2;

import java.util.stream.IntStream;

import ass4.es2.TextLib.Color;

/**
 * Simple rectangle.
 * @author acco
 *
 */
public class Rectangle {

	private static final int X_OFFSET = 1;
	private static final int Y_OFFSET = 2;

	private int height;
	private int width;
	private int y;
	private int x;
	private Color color;
	private TextLib lib;

	Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = Color.GREEN;
		this.lib = TextLibFactory.getInstance();
	}

	public void draw() {
		// draw the horizontal lines
		IntStream.rangeClosed(this.x, this.x + this.width).forEach(i -> {
			lib.writeAt(i, this.y, "=", color);
			lib.writeAt(i, this.y + this.height, "=", color);
		});

		// draw the vertical lines
		IntStream.rangeClosed(this.y, this.y + this.height).forEach(i -> {
			lib.writeAt(this.x, i, "|", color);
			lib.writeAt(this.x + this.width, i, "|", color);
		});
	}
	
	public int getMaxX() {
		return this.x + this.width - X_OFFSET;
	}

	public int getMinX() {
		return this.x + X_OFFSET;
	}

	public int getMinY() {
		return this.y + Y_OFFSET;
	}

	public int getMaxY() {
		return this.y + this.height - Y_OFFSET;
	}




}
