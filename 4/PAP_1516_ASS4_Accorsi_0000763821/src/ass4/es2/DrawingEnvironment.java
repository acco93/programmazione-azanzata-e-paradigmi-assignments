package ass4.es2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * It represents a drawing environment.
 * @author acco
 *
 */

public class DrawingEnvironment {
	
	// defines the viewport.
	Rectangle bounds;
	// drawables that belongs to this drawing environment
	private List<Drawable> drawables;
	
	private Random rand;

	DrawingEnvironment(int x, int y, int width, int height) {
		this.bounds = new Rectangle(x, y, width, height);
		this.drawables = new ArrayList<>();
		rand = new Random();
	}

	public void addDrawable(Drawable drawable){

		// defines the initial position according to the bounds object.
		
		int x = rand.nextInt((bounds.getMaxX() - drawable.getWidth() - bounds.getMinX()) + 1) + bounds.getMinX();
		int y = rand.nextInt((bounds.getMaxY() - bounds.getMinY()) + 1) + bounds.getMinY();
	
		drawable.setX(x);
		drawable.setY(y);
		
		this.drawables.add(drawable);
	}
	
	public void draw() {
		
		bounds.draw();		
		
		// handle each animation with a different thread
		drawables.stream().forEach(drawable -> {
			// add some decorators ...
			new Drawer(new WaveAnimation(new ColorAnimation(new BounceAnimation(new StaticAnimation(drawable),bounds)))).start();
		});

	}


}
