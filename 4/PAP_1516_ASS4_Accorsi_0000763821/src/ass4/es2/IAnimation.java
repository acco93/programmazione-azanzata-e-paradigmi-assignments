package ass4.es2;

/**
 * Animation interface.
 * @author acco
 *
 */
public interface IAnimation {
	
	/**
	 * Draw the animation.
	 */
	void draw();
	
}

