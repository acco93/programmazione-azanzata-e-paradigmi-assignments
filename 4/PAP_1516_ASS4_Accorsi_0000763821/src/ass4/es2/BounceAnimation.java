package ass4.es2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * AbstractAnimation decorator. It causes the drawable to move in a predefined rectangle and bounce when hitting a bound.
 * 
 * @author acco
 *
 */

public class BounceAnimation extends AbstractAnimation {
	
	public enum VerticalDirection {UP, DOWN};
	public enum HorizontalDirection {LEFT, RIGHT};
	
	// the inner animation object
	private AbstractAnimation animation;
	
	// movement variables
	private HorizontalDirection xDirection;
	private VerticalDirection yDirection;
	
	private List<Pair<Integer,Integer>> clearList;
	
	private Drawable drawable;
	
	private Rectangle bounds;

	BounceAnimation(AbstractAnimation animation, Rectangle bounds){
		this.animation = animation;
		this.bounds = bounds;
		this.drawable = this.animation.getDrawable();
		this.xDirection = HorizontalDirection.RIGHT;
		this.yDirection = VerticalDirection.UP;
		this.clearList = new ArrayList<>();
	}
	
	@Override
	protected void render() {
		
		this.animation.render();
		
	}

	@Override
	protected void clear() {
		// cancello le caselle di schermo dove era la parola prima del movimento
		this.clearList.stream().forEach(p -> TextLibFactory.getInstance().writeAt(p.first(),p.second()," "));
		this.animation.clear();
		this.clearList = new ArrayList<>();
	}

	@Override
	protected void transform() {
		
		this.animation.transform();
		
		int x = this.drawable.getX();
		int y = this.drawable.getY();
		
		
		if(this.xDirection == HorizontalDirection.RIGHT){
			// se vado verso destra
			if(x+this.drawable.getWidth() < this.bounds.getMaxX()){
				// se rimango all'interno della viewport (dx)
				this.clearList.add(new Pair<>(this.drawable.getX(), this.drawable.getY()));
				this.drawable.setX(x+1);
			} else {
				// se andando oltre verso destra uscirei
				
				this.xDirection = HorizontalDirection.LEFT;
				this.yDirection = this.yDirection == VerticalDirection.UP? VerticalDirection.UP: VerticalDirection.DOWN;
			}
		} else {
			// se vado verso sinistra
			if(x > this.bounds.getMinX()){
				// se rimango all'interno della viewport (sx)
				this.clearList.add(new Pair<>(this.drawable.getWidth()-1, this.drawable.getY()));
				this.drawable.setX(x-1);
			} else {
				// se andando oltre verso sinistra uscirei 
				
				this.xDirection = HorizontalDirection.RIGHT;
				this.yDirection = this.yDirection == VerticalDirection.UP? VerticalDirection.UP: VerticalDirection.DOWN;
			}
		}
		
		if(this.yDirection == VerticalDirection.UP){
			// se vado verso l'alto
			if(y > this.bounds.getMinY()){
				// se rimango all'interno della viewport (up)
				IntStream.range(0, this.drawable.getWidth()).forEach(
						i->this.clearList.add(new Pair<>(this.drawable.getX()+i, this.drawable.getY()-1)));
				this.drawable.setY(y-1);
			} else {
				// se andando oltre verso l'alto uscirei
				
				IntStream.range(0, this.drawable.getWidth()).forEach(
						i->this.clearList.add(new Pair<>(this.drawable.getX()+i, this.drawable.getY())));
				
				this.yDirection = VerticalDirection.DOWN;
				this.xDirection = this.xDirection == HorizontalDirection.LEFT? HorizontalDirection.LEFT: HorizontalDirection.RIGHT;
			}
		} else {
			// se vado verso il basso
			if(y < this.bounds.getMaxY()){
				// se rimango all'interno della viewport (down)
				IntStream.range(0, this.drawable.getWidth()).forEach(
						i->this.clearList.add(new Pair<>(this.drawable.getX()+i, this.drawable.getY()+1)));
				this.drawable.setY(y+1);
			} else {
				// se andando oltre verso il basso uscirei 

				IntStream.range(0, this.drawable.getWidth()).forEach(
						i->this.clearList.add(new Pair<>(this.drawable.getX()+i, this.drawable.getY())));
				
				this.yDirection = VerticalDirection.UP;
				this.xDirection = this.xDirection == HorizontalDirection.LEFT? HorizontalDirection.LEFT: HorizontalDirection.RIGHT;
			}
		}
		
		
		
	}

	private VerticalDirection randomYDirection() {
		return new Random().nextDouble() >= 0.5? VerticalDirection.DOWN : VerticalDirection.UP;
	}
	
	private HorizontalDirection randomXDirection() {
		return new Random().nextDouble() >= 0.5? HorizontalDirection.LEFT : HorizontalDirection.RIGHT;
	}
	
	@Override
	protected Drawable getDrawable() {
		return this.drawable;
	}
	
}
