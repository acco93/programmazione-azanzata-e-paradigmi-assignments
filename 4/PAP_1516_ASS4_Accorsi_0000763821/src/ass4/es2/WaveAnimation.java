package ass4.es2;

/**
 * AbstractAnimation decorator. It causes waving effect on the animation object.
 * @author acco
 * 
 */

public class WaveAnimation extends AbstractAnimation{

	private AbstractAnimation animation;
	
	private int firstIndex;
	private int midIndex;
	private int lastIndex;

	private char firstChar;
	private char midChar;
	private char lastChar;

	private Drawable drawable;

	WaveAnimation(AbstractAnimation animation){
		this.animation = animation;
		this.drawable = this.animation.getDrawable();
		this.firstIndex = 0;
		this.midIndex = 2;
		this.lastIndex = 4;
	}
	
	@Override
	protected void render() {
		this.animation.render();
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.firstIndex, this.drawable.getY(), " ");
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.firstIndex, this.drawable.getY()+1, firstChar+"",this.drawable.getColor());
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.midIndex, this.drawable.getY(), " ");
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.midIndex, this.drawable.getY()-1, midChar+"",this.drawable.getColor());
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.lastIndex, this.drawable.getY(), " ");
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.lastIndex, this.drawable.getY()+1, lastChar+"",this.drawable.getColor());
	}

	@Override
	protected void clear() {
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.firstIndex, this.drawable.getY()+1, " ");
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.midIndex, this.drawable.getY()-1, " ");
		TextLibFactory.getInstance().writeAt(this.drawable.getX()+this.lastIndex, this.drawable.getY()+1, " ");
		this.animation.clear();
	}

	@Override
	protected void transform() {
		
		this.animation.transform();
		
		// just a little trick ...
		String word = ((Word)this.drawable).getWord();
		
		this.firstIndex++;
		this.firstIndex%=drawable.getWidth();
		this.firstChar = word.charAt(firstIndex);
		
		this.midIndex++;
		this.midIndex%=drawable.getWidth();
		this.midChar = word.charAt(midIndex);
		
		this.lastIndex++;
		this.lastIndex%=drawable.getWidth();
		this.lastChar = word.charAt(lastIndex);
	}

	@Override
	protected Drawable getDrawable() {
		return this.drawable;
	}

}
