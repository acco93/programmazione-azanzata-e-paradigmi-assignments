-- Assignment 2

import Screen

-- es 1
data Sym = Dot | Dash
data Code = Single Sym | Comp Sym Code

longestDashSeq :: Code -> Maybe Int
-- casi base
longestDashSeq (Single Dash) = Just 1
longestDashSeq (Single Dot) = Nothing
-- passo ricorsivo, ho una struttura tipo .--..-
longestDashSeq (Comp sym code) =
  let
    -- calcolo la sequenza più lunga (fino ad un Dot) di Dash in [Dot, Dash, Dash, Dot, Dash]
    left = maybeCountDashTillDot (Comp sym code)
    -- calcolo la sequenza più lunga (fino ad un Dot) di Dash in      [Dash, Dash, Dot, Dash]
    right = longestDashSeq code
  in
    -- restituisco il valore maggiore
    if left > right then left else right

-- funzione di appoggio che trasforma Int in Maybe Int, utilizzata perchè non è possibile sommare Maybe Int
maybeCountDashTillDot :: Code -> Maybe Int
maybeCountDashTillDot code =
  let
    dashSeq = countDashTillDot code
  in
    if dashSeq == 0
      then Nothing
      else Just dashSeq

-- dato un Code conto la prima sequenza di Dash più lunga incontrata (mi fermo al primo Dot)
countDashTillDot :: Code -> Int
countDashTillDot (Single Dash) = 1
countDashTillDot (Single Dot) = 0
countDashTillDot (Comp Dash code) = 1 + countDashTillDot code
countDashTillDot (Comp Dot code) = 0

-- es 2
-- (a) mappo l'array da Int a Maybe Int
-- (b) cerco il massimo inizializzando la ricerca con Nothing (Nothing < Just x per ogni x)
findMax :: [Int] -> Maybe Int
findMax l = foldr (\x y -> if x > y then x else y) Nothing (map (\x -> Just x) l)

-- es 3

-- definizione dei sinonimi
type BuyerId = String
type City = String
type Year = Int
type Amount = Int

data Buyer = Buyer BuyerId City
    deriving (Show)

data Transaction = Trans BuyerId Year Amount
    deriving (Show)

data DBase = DBase [Buyer] [Transaction]

buyers = [
  Buyer "maria" "Milano",
  Buyer "stefano" "Roma",
  Buyer "laura" "Cesena",
  Buyer "alice" "Cesena"]

transactions = [
    Trans "alice" 2011 300,
    Trans "maria" 2012 1000,
    Trans "maria" 2011 400,
    Trans "laura" 2012 710,
    Trans "stefano" 2011 700,
    Trans "stefano" 2012 950,
    Trans "alice" 2015 1000,
    Trans "laura" 2016 2000]

db = DBase buyers transactions

-- Funzioni per estrarre info dalle Transactions & Buyers

getYear :: Transaction -> Year
getYear (Trans buyerId year amount) = year

getAmount :: Transaction -> Amount
getAmount (Trans buyerId year amount) = amount

getBuyerIdFromTransaction :: Transaction -> BuyerId
getBuyerIdFromTransaction (Trans buyerId year amount) = buyerId

getBuyerId :: Buyer -> BuyerId
getBuyerId (Buyer buyerId city) = buyerId

getBuyerCity :: Buyer -> City
getBuyerCity (Buyer buyerId city) = city

-- quicksort con funzione di comparazione
quicksort :: [a] -> (a -> a -> Bool) -> [a]
quicksort [] _ = []
quicksort (x:xs) f =
  let
    ls = quicksort [a | a <- xs, not(f a x) ] f
    rs = quicksort [b | b <- xs, f b x ] f
  in
    ls ++ [x] ++ rs

-- Ottiene la lista delle transazioni riferite all’anno specificato, ordinate per amount
querySortedTransList :: DBase -> Year -> [Transaction]
querySortedTransList (DBase _ transactions) year =
  -- estraggo le transazioni con anno indicato in year tramite list comprehension dopodichè le ordino in base alla lambda exp
  quicksort [t | t <- transactions, getYear(t) == year] (\x y -> if getAmount(x) > getAmount(y) then True else False)

-- funzione di appoggio per sapere se una lista contiene un certo elemento
has :: (Eq a) => [a] -> a -> Bool
has [] _ = False
has (x:xs) a
  | x == a    = True
  | otherwise = has xs a

-- Ottiene l’elenco delle città distinte dei buyer
queryBuyerCities :: DBase -> [City]
-- tramite foldr riduco il set di città mantenendo solo quelle distinte
queryBuyerCities (DBase buyers _) = foldr (\x l -> if has l x then l else x:l) [] cities where
  -- estraggo tutte le città (con eventuali duplicati)
  cities = (map (\x -> getBuyerCity x) buyers)

-- Verifica se esistono buyer di una certa città
queryExistBuyerFrom :: DBase -> City -> Bool
queryExistBuyerFrom (DBase buyers _) city = has cities city where
  cities = (map (\x -> getBuyerCity x) buyers)

-- Determina la somma delle transazioni eseguite da buyer di una certa città
queryAmountsFromCity :: DBase -> City -> Amount
queryAmountsFromCity (DBase buyers transactions) city = foldr (\t tot -> (getAmount t) + tot) 0 cityTransactions where
  -- recupero gli id delle persone della città selezionata (filtro e recupero solo gli id)
  cityBuyersId = map (\b -> getBuyerId b) (filter (\b -> getBuyerCity b == city) buyers)
  -- recupero le transazioni associate agli id precedentemente filtrati
  cityTransactions = filter (\t -> has cityBuyersId (getBuyerIdFromTransaction t)) transactions

-- es 4

instance Show Color where
  show BLACK = "BLACK"
  show RED = "RED"
  show GREEN = "GREEN"
  show YELLOW = "YELLOW"
  show BLUE = "BLUE"
  show MAGENTA = "MAGENTA"
  show CYAN = "CYAN"
  show WHITE = "WHITE"

data TObj = -- TextualString: testo, colore, posizione
            TString String Color Pos |
            -- HorizontalLine: lunghezza, posizione, colore
            HLine Int Pos Color |
            -- VerticalLine: lunghezza, posizione, colore
            VLine Int Pos Color |
            -- EmptyBox: vertice top-left, larghezza, altezza, colore
            EBox Pos Int Int Color |
            -- FullBox: vertice top-left, larghezza, altezza, colore
            FBox Pos Int Int Color
            deriving Show

-- definisco la classe Viewable la quale ha nell'interfaccia le seguenti funzioni
class Viewable o where
  render :: Viewport -> o ->  IO ()
  renderWithClipping :: Viewport -> o ->  IO ()
  renderAll :: Viewport -> [o] ->  IO ()
  renderAllC :: Viewport -> [(o,Bool)] -> IO()

-- rendo TObj un'istanza di Viewable e definisco l'implementazione delle funzioni
instance Viewable TObj where

  -- rendering della TextualString
  render (Viewport (left,top) width height) (TString string color (x,y)) =
    drawViewport (Viewport (left,top) width height) >> setFColor color >> writeAt (left+x,top+y) string >> setFColor WHITE

  -- rendering dell'HorizontalLine
  render (Viewport (left,top) width height) (HLine len (x,y) color) =
    drawViewport (Viewport (left,top) width height) >> setFColor color >> loopHorizontalWrite (left+x,top+y) 0 len "-" >> setFColor WHITE

  -- rendering della VerticalLine
  render (Viewport (left,top) width height) (VLine len (x,y) color) =
    drawViewport (Viewport (left,top) width height) >> setFColor color >> loopVerticalWrite (left+x,top+y) 0 len "|" >> setFColor WHITE

  -- rendering dell'EmptyBox
  render (Viewport (left,top) width height) (EBox (x,y) w h color) =
    drawViewport (Viewport (left,top) width height) >> setFColor color >> drawBox (left+x,top+y) w h False >> setFColor WHITE

  -- rendering del FullBox
  render (Viewport (left,top) width height) (FBox (x,y) w h color) =
    drawViewport (Viewport (left,top) width height) >> setFColor color >> drawBox (left+x,top+y) w h True >> setFColor WHITE


  -- rendering con clipping della TextualString
  renderWithClipping (Viewport (left,top) width height) (TString string color (x,y)) =
    drawViewport (Viewport (left,top) width height) >> setFColor color >>
    writeStringWithClipping (Viewport (left,top) width height) string (x,y)
    >> setFColor WHITE

  -- rendering con clipping dell'HorizontalLine
  renderWithClipping (Viewport (left,top) width height) (HLine len (x,y) color)
    -- elimino le linee orizzontali sopra o sotto alla viewport
    | y < 0 || y > height = return ()
    -- ridimensiono la linea clippando ciò che è esterno alla viewport
    | otherwise = render (Viewport (left,top) width height) (HLine actualLen (actualX,y) color) where
      actualX = if x < 0 then 0 else x
      actualLen
        -- caso in cui entrambi gli estremi escono dalla viewport
        | x < 0 && len > width = width
        -- caso in cui l'estremo sx esce dalla viewport (x è negativo! perciò sommo)
        | x < 0 && len < width = len+x
        -- caso in cui entrambi gli estremi sono interni alla viewport
        | x >= 0 && len <= width = len
        -- caso in cui l'estremo dx esce dalla viewport
        | otherwise = width-actualX

  -- rendering con clipping della VerticalLine
  renderWithClipping (Viewport (left,top) width height) (VLine len (x,y) color)
    -- elimino le linee verticali ai lati della viewport
    | x < 0 || x > width = return ()
    -- ridimensiono la linea clippando ciò che è esterno alla viewport
    | otherwise = render (Viewport (left,top) width height) (VLine actualLen (x,actualY) color) where
        actualY = if y < 0 then 0 else y
        actualLen
          | y < 0 && len > height = height
          | y < 0 && len < height = len+y
          | y >=0 && len <= height = len
          | otherwise = height-actualY

  -- rendering con clipping di un EmptyBox
  renderWithClipping (Viewport (left,top) width height)  (EBox (x,y) w h color) =
    render (Viewport (left,top) width height) (EBox (actualX,actualY) actualW actualH color) where
      -- ridimensiono il rettangolo clippando ciò che è esterno alla viewport
      actualX = if x < 0 then 0 else x
      actualY = if y < 0 then 0 else y
      actualW
        | x < 0 && w > width = width
        | x < 0 && w < width = w+x
        | x >= 0 && w <= width = w
        | otherwise = width-actualX
      actualH
        | y < 0 && h > height = height
        | y < 0 && h < height = h+y
        | y >=0 && h <= height = h
        | otherwise = height-actualY

  -- rendering con clipping di un FullBox
  renderWithClipping (Viewport (left,top) width height)  (FBox (x,y) w h color) =
    render (Viewport (left,top) width height) (FBox (actualX,actualY) actualW actualH color) where
      -- ridimensiono il rettangolo clippando ciò che è esterno alla viewport
      actualX = if x < 0 then 0 else x
      actualY = if y < 0 then 0 else y
      actualW
        | x < 0 && w > width = width
        | x < 0 && w < width = w+x
        | x >= 0 && w <= width = w
        | otherwise = width-actualX
      actualH
        | y < 0 && h > height = height
        | y < 0 && h < height = h+y
        | y >=0 && h <= height = h
        | otherwise = height-actualY


  -- rendering senza clipping di una lista di TObj
  renderAll _ [] = return ()
  renderAll v (x:xs) = render v x >> renderAll v xs

  -- rendering (con scelta clipping o no (parametro c per ogni elemento)) di una lista di TObj
  renderAllC _ [] = return ()
  renderAllC v ((x,c):xs)
    | c == True = renderWithClipping v x >> renderAllC v xs
    | otherwise = render v x >> renderAllC v xs

-- ritorna la lista con Int elementi rimossi dal fondo (utilizzata per ridurre la stringa)
-- Parametri: Viewport, lista di caratteri, coordinata x della lista
writeStringWithClipping :: Viewport -> String -> Pos -> IO()
writeStringWithClipping (Viewport (left,top) width height) (s:xs) (x,y) = writeStringWithClippingLoop (left,top) width (s:xs) (x,y) where
  writeStringWithClippingLoop _ _ [] _ = return ()
  writeStringWithClippingLoop (vLeft,vTop) vWidth (s:t) (x,y)
      -- scarto il carattere se è fuori dalla viewport a sx
    | x < 0 = writeStringWithClippingLoop (vLeft,vTop) vWidth t (x+1,y)
    | x > vWidth = return ()
      -- prendo il carattere se è nella viewport
    | otherwise = writeAt (x+vLeft,y+vTop) [s] >> writeStringWithClippingLoop (vLeft,vTop) vWidth t (x+1,y)
      -- posso fermarmi appena esco a dx dalla viewport

-- Parametri: left-top, indiceCorrente, lunghezza, stringa da stampare
-- stampo orizzontalmente n volte la stringa passata
loopHorizontalWrite :: Pos -> Int -> Int -> String -> IO()
loopHorizontalWrite (x,y) i n s
  | i == n = return ()
  | otherwise = writeAt (x+i,y) s >> loopHorizontalWrite (x,y) (i+1) n s

-- Parametri: left-top, indiceCorrente, lunghezza, stringa da stampare
-- stampo verticalmente n volte la stringa passata
loopVerticalWrite :: Pos -> Int -> Int -> String -> IO()
loopVerticalWrite (x,y) i n s
  | i == n = return ()
  | otherwise = writeAt (x,y+i) s >> loopVerticalWrite (x,y) (i+1) n s

-- disegna la viewport
drawViewport :: Viewport -> IO ()
drawViewport (Viewport (left,top) width height) =
  setFColor GREEN >>
  writeAt (left+(quot width 2)-4,top-1) "VIEWPORT" >>
  drawBox (left,top) width height False >>
  setFColor WHITE

-- disegna un rettangolo (vuoto o pieno)
-- Parametri: top-left, width, height, fill
drawBox :: Pos -> Int -> Int -> Bool -> IO()
drawBox (left,top) width height fill
    -- caso base: mi permette di fermare la ricorsione
  | width <= 0 || height <= 0 = return ()
    -- rettangolo vuoto
  | fill == False = drawIt
    -- rettangolo pieno: disegno un rettangolo vuoto e mi chiamo ricorsivamente per disegnare un rettangolo vuoto dentro di me
    -- fino a creare un rettangolo pieno
  | otherwise = drawIt >> drawBox (left+1,top+1) (width-2) (height-2) True
    where
    drawIt =  loopHorizontalWrite (left+1, top) 0 (width-1) "=" >>
              loopHorizontalWrite (left+1, top+height) 0 (width-1) "=" >>
              loopVerticalWrite (left, top+1) 0 (height-1) "|" >>
              loopVerticalWrite (left+width, top+1) 0 (height-1) "|"

-- test esercizio 4
testTStrings :: IO()
testTStrings =  cls >> cls >> cls >>
                renderAllC v [(s,False),(s',True),(s'',False),(s''',True),(s'''',False)] >>
                goto (0,0) where
  v = Viewport (50,10) 50 20
  s = TString "Hello world! :)" RED (15,3)
  s' = TString "Hello world! :)" BLUE (40,7)
  s'' = TString "Hello world! :)" WHITE (40,14)
  s''' = TString " Nel mezzo del cammin di nostra vita mi ritrovai per una selva oscura ché la diritta via era smarrita." YELLOW (-10,10)
  s'''' = TString " Nel mezzo del cammin di nostra vita mi ritrovai per una selva oscura ché la diritta via era smarrita." YELLOW (-10,16)

testLines :: IO()
testLines = cls >> cls >> cls >>
            renderAllC v [(hl,False),(hl',True),(hl'', False),(vl,False),(vl',True),(vl'',False)]  >>
            goto (0,0) where
  v = Viewport (50,10) 50 20
  hl = HLine 5 (3,2) RED
  hl' = HLine 10 (-5,5) CYAN
  hl'' = HLine 20 (40,8) YELLOW
  vl = VLine 5 (5,4) MAGENTA
  vl' = VLine 10 (15,13) CYAN
  vl'' = VLine 10 (30,13) WHITE

testBoxes :: IO()
testBoxes = cls >> cls >> cls >>
            renderAllC v [(eb,False),(eb',True),(eb'', False),(fb,False),(fb',True),(fb'',False),(s,False),(s',True)]  >>
            goto (0,0) where
  v = Viewport (40,5) 80 25
  eb = EBox (40,2) 7 5 RED
  eb' = EBox (74,3) 10 10 CYAN
  eb'' = EBox (-3,20) 10 10 YELLOW
  fb = FBox (45,9) 7 5 MAGENTA
  fb' = FBox (-6,-5) 10 10 CYAN
  fb'' = FBox (60,20) 10 10 WHITE
  s = TString "Hello world! :)" RED (15,3)
  s' = TString " Nel mezzo del cammin di nostra vita mi ritrovai per una selva oscura ché la diritta via era smarrita. Ahi quanto a dir qual era è cosa dura esta selva selvaggia e aspra e forte che nel pensier rinova la paura!" YELLOW (-2,18)
