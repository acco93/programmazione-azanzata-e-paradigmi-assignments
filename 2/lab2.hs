-- es 1
type Word = String
mapLen :: [Word] -> [(Word,Int)]
mapLen l = map (\x -> (x,len x)) l where
  len w = foldr (\_ c -> c+1) 0 w

-- tramite list comprehension
mapLenAlt :: [Word] -> [(Word,Int)]
mapLenAlt l = [ (w,length w) | w<-l , w /="gatto"]

-- es 2
selectedLen :: [Word] -> Int -> [(Word,Int)]
-- è una classica chiusura, x è una var libera
selectedLen l x = filter (\(_,len) -> len > x) (mapLen l)

selectedLenAlt :: [Word] -> Int -> [(Word,Int)]
-- length w viene calcolato due volte perchè lui nn sa che sn la stessa cosa, meglio mettere un ambiente locale (non si può...)
selectedLenAlt l x = [ (w,length w) | w<-l , length w > x]

-- come composizione di funz
selectedLenAltAlt :: Int -> ([Word]->[(Word,Int)])
-- è una classica chiusura, x è una var libera
selectedLenAltAlt x = (filter (\(_,len) -> len > x) . mapLen)
-- non passiamo niente

-- es 3
wordOcc :: [Word] -> Word -> Int
wordOcc l w = foldr (\w' c -> if (w' == w) then c+1 else c) 0 l

-- es 4
wordsOcc :: [Word] -> [(Int,[Word])]

wordsOcc l = (myFold . getOccs) l

getOccs l = map (\w -> (wordOcc l w, w)) l
myFold l = foldr (\occ l' -> updateList occ l') [] l where
  updateList (o, w) [] = [(o,[w])]
  updateList (o, w) ((o',lw):xs)
    | o == o' && not (member w lw) = (o,w:lw):xs
    | o == o' && (member w lw)  = (o,lw):xs
    | otherwise = (o', lw):updateList (o,w) xs

member _ [] = False
member v (x:xs)
  | v == x  = True
  | otherwise = member v xs

-- Es 5

printElems :: [String] -> IO()
printElems [] = return ()
printElems (x:xs) = (putStrLn ("\t"++x)) >> printElems xs
