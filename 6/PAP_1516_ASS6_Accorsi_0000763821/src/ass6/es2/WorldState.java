package ass6.es2;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * World state implementation.
 * 
 * @author acco
 *
 */
public class WorldState {

	
	private CellState[][] matrix;
	
	private int livingCells;
	private int generation;

	public WorldState(int generation) {
		this.livingCells = 0;
		this.generation = generation;
		this.matrix = new CellState[World.ROWS][World.COLUMNS];
	}
	
	
	
	public int getGeneration() {
		return generation;
	}



	public int getLivingCells() {
		return livingCells;
	}



	public void setLivingCells(int livingCells) {
		this.livingCells = livingCells;
	}



	public void alive(int i, int j){
		this.set(i, j, CellState.ALIVE);
	}
	
	public void dead(int i, int j){
		this.set(i, j, CellState.DEAD);
	}
	
	private void set(int i, int j, CellState state){
		this.matrix[i][j] = state;
	}


	/*
	 * 
	 * neighbors + evolveOld -> much more inefficient approach but better in style...
	 * 
	 */
	
	private List<CellState> neighbors(int i, int j){
		List<CellState> n = new ArrayList<>();
		
		/*
		 * x = state[i,j]
		 * 
		 * 		A	B	C
		 *		D	x	E 
		 * 		F	G	H
		 */
		
		int up = (i - 1 + World.ROWS) % World.ROWS;
		int down = (i + 1) % World.ROWS;
		int left = (j - 1 + World.COLUMNS) % World.COLUMNS;
		int right = (j + 1) % World.COLUMNS;
		
		// A
		n.add(matrix[up][left]);
		// B
		n.add(matrix[up][j]);
		// C
		n.add(matrix[up][right]);
		// D
		n.add(matrix[i][left]);
		// E
		n.add(matrix[i][right]);
		// F
		n.add(matrix[down][left]);
		// G
		n.add(matrix[down][j]);
		// H
		n.add(matrix[down][right]);
		
		return n;
	}

	public CellState evolveOld(int i, int j){
		int livingNeighbors = (int) this.neighbors(i, j)
										.stream()
										.filter(c -> c == CellState.ALIVE)
										.count();

		return livingNeighbors == 2? CellState.ALIVE : CellState.DEAD;
	}
	
	public CellState evolve(int i, int j){
		
		int sum = 0;
		
		int up = (i - 1 + World.ROWS) % World.ROWS;
		int down = (i + 1) % World.ROWS;
		int left = (j - 1 + World.COLUMNS) % World.COLUMNS;
		int right = (j + 1) % World.COLUMNS;
		
		if(matrix[up][left] == CellState.ALIVE) {sum++;}
		// B
		if(matrix[up][j]== CellState.ALIVE) {sum++;}
		// C
		if(matrix[up][right]== CellState.ALIVE) {sum++;}
		// D
		if(matrix[i][left]== CellState.ALIVE) {sum++;}
		// E
		if(matrix[i][right]== CellState.ALIVE) {sum++;}
		// F
		if(matrix[down][left]== CellState.ALIVE) {sum++;}
		// G
		if(matrix[down][j]== CellState.ALIVE) {sum++;}
		// H
		if(matrix[down][right]== CellState.ALIVE) {sum++;}
		
		return sum == 2? CellState.ALIVE : CellState.DEAD;
		
	}
	

	
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder();
		string.append("World ["+World.ROWS+"x"+World.COLUMNS+"]\n");
		for(int i=0;i<World.ROWS;i++){
			for(int j=0;j<World.COLUMNS;j++){
				if(this.matrix[i][j]==CellState.ALIVE){
					string.append(" x ");	
				} else {
					string.append("   ");
				}
			}
			string.append("\n");
		}
		return string.toString();
	}

	public CellState get(int i, int j) {
		return this.matrix[i][j];
	}
	
}
