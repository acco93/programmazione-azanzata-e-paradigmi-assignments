package ass6.es2;

import java.util.Random;


/**
 * 
 * Initial configuration example.
 * 
 * @author acco
 *
 */
public class RandomWorldState extends WorldState {

	public RandomWorldState(int generation) {
		super(generation);
		
		Random r = new Random();
		
		int lc=0;
		
		for(int i=0;i<World.ROWS; i++){
			for(int j=0;j<World.COLUMNS;j++){
				if(r.nextDouble()>0.99){
					this.alive(i, j);
					lc++;
				}else{
					this.dead(i, j);
				}
			}
		}
		
		this.setLivingCells(lc);

	}

}
