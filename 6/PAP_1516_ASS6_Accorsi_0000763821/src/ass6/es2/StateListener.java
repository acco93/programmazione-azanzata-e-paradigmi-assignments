package ass6.es2;


/**
 * 
 * Interface implemented by whom wants to be notified when
 * the application state changes.
 * 
 * @author acco
 *
 */

public interface StateListener {
	
	void stateNotification(ProgramState ps);

}
