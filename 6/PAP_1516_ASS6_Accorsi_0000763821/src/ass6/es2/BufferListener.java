package ass6.es2;

/**
 * 
 * Interface implemented by whom wants to be notified when
 * a Master object puts an element (world state) in the buffer.
 * 
 * @author acco
 *
 */
public interface BufferListener {

	/**
	 * Notify the buffer observer.
	 * @param bufferElems elements in the buffer array
	 */
	void bufferNotification(int bufferElems);
	
}
