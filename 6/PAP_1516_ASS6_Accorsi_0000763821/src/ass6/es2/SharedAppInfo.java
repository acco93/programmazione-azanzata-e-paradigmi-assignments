package ass6.es2;

/**
 * Application state info wrapped in a monitor.
 * @author acco
 *
 */

public class SharedAppInfo {
	
	private boolean isPaused;
	private boolean isStopped;

	public SharedAppInfo(){
		this.isPaused = false;
		this.isStopped = false;
	}

	public synchronized boolean isPaused() {
		return isPaused;
	}

	public synchronized void setPaused(boolean isPaused) {
		this.isPaused = isPaused;
	}

	public synchronized boolean isStopped() {
		return isStopped;
	}

	public synchronized void setStopped(boolean isStopped) {
		this.isStopped = isStopped;
	}
	
	
	
}

