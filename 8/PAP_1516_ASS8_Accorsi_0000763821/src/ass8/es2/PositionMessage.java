package ass8.es2;

import pap.ass08.pos.P2d;

/**
 * Position related information.
 * 
 * @author acco
 *
 */
public class PositionMessage {

	private P2d currentPosition;
	private double speed;
	private double averageSpeed;

	public PositionMessage(P2d currentPosition, double speed, double averageSpeed) {
		this.currentPosition = currentPosition;
		this.speed = speed;
		this.averageSpeed = averageSpeed;
	}

	public P2d getCurrentPosition() {
		return currentPosition;
	}

	public double getSpeed() {
		return speed;
	}

	public double getAverageSpeed() {
		return averageSpeed;
	}

}
