package ass8.es2;

import pap.ass08.pos.P2d;

/**
 * 
 * A pair with position & highest hb value till now.
 * 
 * @author acco
 *
 */
public class PositionWithMaxHbValueMessage {

	private final int hbValue;
	private final P2d posValue;

	public PositionWithMaxHbValueMessage(int hbValue, P2d posValue) {
		this.hbValue = hbValue;
		this.posValue = posValue;
	}

	public int getHbValue() {
		return hbValue;
	}

	public P2d getPosValue() {
		return posValue;
	}
	
	

}
