package ass8.es2;

import pap.ass08.pos.P2d;

/**
 * 
 * View interface.
 * 
 * @author acco
 *
 */
public interface View {

	/**
	 * Set the current hb value.
	 * 
	 * @param value
	 */
	void setCurrentHBValue(int value);

	/**
	 * Set the average hb value.
	 * 
	 * @param value
	 */
	void setAvgHBValue(int value);

	/**
	 * Trigger the alarm.
	 */
	void setHBAlartm();

	/**
	 * Shutdown the alarm.
	 */
	void unsetHBAlarm();

	/**
	 * Set the current position.
	 * 
	 * @param p2d
	 */
	void setCurrentPosition(P2d p2d);

	/**
	 * Notify the instantaneous velocity.
	 * 
	 * @param speed
	 */
	void notifyCurrentSpeed(double speed);

	/**
	 * Notify the average speed.
	 * 
	 * @param avgSpeed
	 */
	void notifyAvgSpeed(double avgSpeed);

	/**
	 * Set the position with maximum hb value.
	 * 
	 * @param hbValue
	 * @param posValue
	 */
	void setPositionWithMaxHbValue(int hbValue, P2d posValue);

}
