package ass8.es2;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.japi.pf.ReceiveBuilder;

import pap.ass08.pos.P2d;

/**
 * 
 * The displayer actor handles & update the UI according to the messages
 * received. It could also ask to update the detector & inspector behavior (edit
 * threshold/alarm time, pause/resume detection) obviously via messages.
 * 
 * @author acco
 *
 */
public class DisplayerActor extends AbstractActor {

	private ActorSelection detector;
	private ActorSelection inspector;

	public DisplayerActor() {

		detector = this.context().actorSelection("../" + Register.DETECTOR_ACTOR);
		inspector = this.context().actorSelection("../" + Register.INSPECTOR_ACTOR);

		View view = new FormView(this);

		this.receive(ReceiveBuilder
				/*
				 * Update the current position.
				 */
				.match(PositionMessage.class, m -> {
					view.setCurrentPosition(this.mapToViewCoordinates(m.getCurrentPosition()));
					view.notifyCurrentSpeed(m.getSpeed());
					view.notifyAvgSpeed(m.getAverageSpeed());
				})
				/*
				 * Update the position with max hb value.
				 */
				.match(PositionWithMaxHbValueMessage.class, m -> {
					view.setPositionWithMaxHbValue(m.getHbValue(), this.mapToViewCoordinates(m.getPosValue()));
				})
				/*
				 * Update hb related info.
				 */
				.match(HeartMessage.class, m -> {
					view.setAvgHBValue(m.getAverageValue());
					view.setCurrentHBValue(m.getCurrentValue());
					if (m.isAlarm()) {
						view.setHBAlartm();
					} else {
						view.unsetHBAlarm();
					}
				}).matchAny(o -> {
					this.unhandled(o);
				}).build());

	}

	/**
	 * Adjust coordinates according to the form coordinates system.
	 * 
	 * @param p
	 * @return
	 */
	private P2d mapToViewCoordinates(P2d p) {
		double x = p.getX() + FormView.WIDTH / 2;
		double y = p.getY() + FormView.HEIGHT / 2;
		return new P2d(x, y);
	}

	/**
	 * UI -> actor interaction: ask the detector to resume the detection.
	 */
	public void start() {
		detector.tell(new ResumeMessage(), self());
	}

	/**
	 * UI -> actor interaction: ask the detector to pause the detection.
	 */
	public void stop() {
		detector.tell(new PauseMessage(), self());
	}

	/**
	 * UI -> actor interaction: ask the inspector to edit the alarm threshold.
	 */
	public void setHBThreshold(int th) {
		inspector.tell(new ThresholdMessage(th), self());
	}

	/**
	 * UI -> actor interaction: ask the inspector to edit the alarm time.
	 */
	public void setHBAlarmTime(int time) {
		inspector.tell(new AlarmTimeMessage(time), self());
	}

}
