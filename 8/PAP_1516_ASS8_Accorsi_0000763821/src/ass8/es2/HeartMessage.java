package ass8.es2;

/**
 * 
 * It contains heart related information.
 * 
 * @author acco
 *
 */
public class HeartMessage {

	private final int currentValue;
	private final int averageValue;
	private final boolean alarm;

	public HeartMessage(int currentValue, int averageValue, boolean alarm) {
		this.currentValue = currentValue;
		this.averageValue = averageValue;
		this.alarm = alarm;
	}

	public int getCurrentValue() {
		return currentValue;
	}

	public int getAverageValue() {
		return averageValue;
	}

	public boolean isAlarm() {
		return alarm;
	}
	
	

}
