package ass8.es2;

/**
 * 
 * Utility class that provides a jobs register functionality.
 * 
 * @author acco
 *
 */
public class Register {

	public static final String DETECTOR_ACTOR = "detectorActor";
	public static final String INSPECTOR_ACTOR = "inspectorActor";
	public static final String DISPLAYER_ACTOR = "displayerActor";

}
