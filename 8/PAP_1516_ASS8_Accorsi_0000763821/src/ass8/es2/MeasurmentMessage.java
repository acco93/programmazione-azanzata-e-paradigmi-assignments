package ass8.es2;

import pap.ass08.pos.P2d;

/**
 * 
 * Heartbeat value and position measured at the same instant. The period is
 * necessary to compute the speed, in general the sampling rate could be changed
 * so the inspector needs to know it.
 * 
 * @author acco
 *
 */
public class MeasurmentMessage {

	private final int heartbeatValue;
	private final P2d position;
	private int period;

	public MeasurmentMessage(int heartbeatValue, P2d position, int period) {
		this.heartbeatValue = heartbeatValue;
		this.position = position;
		this.period = period;
	}

	public int getHeartbeatValue() {
		return heartbeatValue;
	}

	public P2d getPosition() {
		return position;
	}

	public int getPeriod() {
		return period;
	}

}
