package ass8.es1;

/**
 * 
 * App entry point.
 * 
 * @author acco
 *
 */
public class Main {

	public static void main(String[] args) {
		new Controller();
	}

}
