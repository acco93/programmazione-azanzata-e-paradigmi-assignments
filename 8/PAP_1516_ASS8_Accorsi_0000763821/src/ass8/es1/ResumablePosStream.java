package ass8.es1;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import pap.ass08.pos.P2d;
import pap.ass08.pos.PosSensor;
import rx.Observable;
import rx.Subscriber;

/**
 * 
 * Position stream implementation (with the pause/resume feature)
 * 
 * @author acco
 *
 */
public class ResumablePosStream {

	private int period;
	private boolean isPaused;
	private PosSensor posSensor;
	private ReentrantLock mutex;
	private Condition cond;

	public ResumablePosStream(int period){
		this.period = period;
		this.isPaused = true;
		this.posSensor = new PosSensor();
		this.mutex = new ReentrantLock();
		this.cond = mutex.newCondition();
	}
	
	public Observable<P2d> makeObservable(){
		Observable<P2d> stream = Observable.create((Subscriber<? super P2d> subscriber) -> {
			new Thread(() -> {
				while (true) {
					try {
						
						mutex.lock();
							while(isPaused){
								cond.await();
							}
						mutex.unlock();
						
						P2d value = this.posSensor.getCurrentValue();
						subscriber.onNext(value);
						Thread.sleep(this.period);
					} catch (Exception ex){}
				}
			}).start();
		});
		return stream;
	}
	
	public void pause(){
		mutex.lock();
		this.isPaused = true;
		mutex.unlock();
	}
	
	public void resume(){
		mutex.lock();
		this.isPaused = false;
		this.cond.signal();
		mutex.unlock();
	}
	
}
