package ass8.es1;

import pap.ass08.pos.P2d;

/**
 * 
 * Fusion between HeartbeatListener & PositionListener.
 * 
 * @author acco
 *
 */
public interface Listener extends HeartbeatListener, PositionListener {

	/**
	 * Notify the position with the maximum hb value.
	 * 
	 * @param hbValue
	 *            hb value
	 * @param posValue
	 *            position
	 */
	void setPositionWithMaxHbValue(int hbValue, P2d posValue);

}
