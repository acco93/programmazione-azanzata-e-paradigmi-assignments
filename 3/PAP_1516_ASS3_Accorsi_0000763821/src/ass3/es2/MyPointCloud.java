package ass3.es2;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MyPointCloud implements PointCloud {

	List<P2d> points;
	
	MyPointCloud(List<P2d> init){
		this.points = init;
	}
	
	@Override
	public void move(V2d v) {
		// traslo ciascun punto del vettore dato
		this.points = this.points.stream().map(p -> p.sum(v)).collect(Collectors.toList());
	}

	@Override
	public List<P2d> getPointsInRegion(Region r) {
		// recupero i punti all'interno della regione
		return points.stream().filter(	p -> 	p.getX() >= r.getUpperLeft().getX() &&
												p.getX() <= r.getBottomRight().getX() &&
												p.getY() >= r.getUpperLeft().getY() &&
												p.getY() <= r.getBottomRight().getY())
								.collect(Collectors.toList());
	}

	@Override
	public Optional<P2d> nearestPoint(P2d p) {
		return points.stream().min((x,y)->(int)(P2d.distance(p,x)-P2d.distance(p,y)));
				
	}

	@Override
	public String toString() {		
		String res = this.points.stream().map(p->p.toString()).reduce("{",(string, point)->string+point+", ");
		if(!this.points.isEmpty()) {
			res=res.substring(0, res.length()-2);
		}
		res+="}";
		return res;
	}
	
	

}
