package ass3.es2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.List;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Frame extends JFrame {

	private static final long serialVersionUID = 3011223377213319383L;

	static final int XMIN = 1;
	static final int YMIN = 1;
	static final int YMAX = 600;
	static final int XMAX = 800;
	
	public Frame(PointCloud pc){
		
		this.setTitle("Points Cloud");
		this.setSize(XMAX, YMAX);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.add(new DrawPanel(pc));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		
	}
	
}

class DrawPanel extends JPanel implements MouseListener, MouseMotionListener{
	
	private static final long serialVersionUID = 5334285979261196224L;
	
	private PointCloud pc;
	
	// punto dove viene cliccato il mouse
	private Optional<P2d> pressedPoint;
	// punto dove viene rilasciato il mouse
	private Optional<P2d> releasedPoint;
	// punto utilizzato per gestire il rettangolo disegnato durante il movimento del mouse
	private Optional<P2d> draggedPoint;
	// punto più vicino a quello premuto
	private Optional<P2d> nearestPoint;
	
	private boolean drawRect;
	private double rectW;
	private double rectY;
	private double rectX;
	private double rectH;



	
	DrawPanel (PointCloud pc){
		this.pc = pc;
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.setBackground(Color.black);
		this.pressedPoint = Optional.empty();
		this.releasedPoint = Optional.empty();
		this.draggedPoint = Optional.empty();
		this.nearestPoint = Optional.empty();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		       		
		g2.setColor(Color.DARK_GRAY);
		
		List<P2d> points = pc.getPointsInRegion(new Region(new P2d(Frame.XMIN-1,Frame.YMIN-1), new P2d(Frame.XMAX+1,Frame.YMAX+1)));
		points.stream().forEach(p->g2.fillOval((int)p.getX(), (int)p.getY(), 5, 5));
		
		if(pressedPoint.isPresent() && releasedPoint.isPresent()){
			List<P2d> pointsInRegion = pc.getPointsInRegion(new Region(pressedPoint.get(),releasedPoint.get()));
			g2.setColor(Color.yellow);
			pointsInRegion.stream().forEach(p->g2.fillOval((int)p.getX(), (int)p.getY(), 5, 5));
		}
		
		nearestPoint.ifPresent(p->{
			g2.setColor(Color.CYAN);
			g2.drawLine((int) (p.getX()+2.5), (int)(p.getY()+2.5), (int)pressedPoint.get().getX(),(int)pressedPoint.get().getY() );
			g2.setColor(Color.WHITE);
			g2.fillOval((int)p.getX(), (int)p.getY(), 5, 5);
			
		});
		
		if(this.drawRect){
			g2.setColor(Color.green);
			g2.drawRect((int)rectX,(int) rectY,(int)rectW,(int)rectH);
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.releasedPoint = Optional.empty();
		this.nearestPoint = Optional.empty();
		repaint();
		this.pressedPoint = Optional.of(new P2d(e.getX(),e.getY()));
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.drawRect = false;
		this.releasedPoint = Optional.of(new P2d(e.getX(),e.getY()));
		
		if(this.releasedPoint.get().equals(this.pressedPoint.get())){
			// se clicco e rilascio nello stesso punto => evidenzo il nearest
			this.nearestPoint = pc.nearestPoint(this.pressedPoint.get());
		} else {
			// altrimenti evidenzio i punti selezionati
			this.fixPoints();	
		}
		
		this.repaint();
	}

	private void fixPoints() {
		double xPressed = this.pressedPoint.get().getX();
		double xReleased = this.releasedPoint.get().getX();
		double yPressed = this.pressedPoint.get().getY();
		double yReleased = this.releasedPoint.get().getY();
		
		double x = Math.min(xReleased, xPressed);
		double w = Math.abs(xPressed - xReleased);

		double y = Math.min(yReleased, yPressed);
		double h = Math.abs(yPressed - yReleased);
		
		this.pressedPoint = Optional.of(new P2d(x,y));
		this.releasedPoint = Optional.of(new P2d(x+w, y+h));
		
	
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {	}

	@Override
	public void mouseExited(MouseEvent e) {		}

	@Override
	public void mouseDragged(MouseEvent e) {
		this.drawRect = true;
		this.draggedPoint = Optional.of(new P2d(e.getX(),e.getY()));
		this.computeRect();

		repaint();

		
	}

	private void computeRect() {
		double xPressed = this.pressedPoint.get().getX();
		double xDragged = this.draggedPoint.get().getX();
		double yPressed = this.pressedPoint.get().getY();
		double yDragged = this.draggedPoint.get().getY();
		
		
		this.rectX = Math.min(xDragged, xPressed);
		this.rectW = Math.abs(xPressed - xDragged);

		this.rectY  = Math.min(yDragged, yPressed);
		this.rectH  = Math.abs(yPressed - yDragged);
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {	}
	
	
}
