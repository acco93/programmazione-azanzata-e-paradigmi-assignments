package ass3.es1;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TextUtils {

	/**
	 * Splitta le parole in corrispondenza dei principali simboli di punteggiatura e ritorna una lista di stringhe
	 * @param text
	 * @return stream
	 */
	private static List<String> textToWordsList(String text) {
		return Arrays.asList(text.split("[ ,.;!?]"));
	}
	
	static List<WordLen> getWordsLength(String text) { 
		// 
		List<WordLen> list = textToWordsList(text).stream()
		// mappo la parola nell'oggetto WorldLen
		.map(w -> new WordLen(w,w.length()))
		// trasformo lo stream in una lista
		.collect(Collectors.toCollection(ArrayList::new));
		return list; 
	};
	
	static Optional<String> getWordWithMaxLen(String text){
		return textToWordsList(text).stream().max((a,b)->a.length()-b.length());
	}


	static int getWordFreq(String text, String word){
		return (int) textToWordsList(text).stream().filter(w -> w.equals(word)).count();
	}
	
	static List<WordPos> getWordsPos(String text){
		
		// return value
		ArrayList<WordPos> list = new ArrayList<>();
		
		// splitto il testo
		List<String> words = textToWordsList(text);
		
		// creo uno stream contenente solo le parole distinte
		Stream<String> distinctWords = words.stream().distinct();
		
		// per ogni parola recupero le sue occorrenze *
		distinctWords.forEach(w -> {
			list.add(getWordPos(words,words.size(),w));
		});
		
		// * non posso passare direttamente lo stream con il mapping (parola,posizione)
		// perché dopo il processing lo stream viene chiuso => devo ricrearlo tutte le volte
		return list;
	}

	
	/**
	 * Dato un testo ed una parola restituisce la rispettiva WordPos (parola + lista occorrenze)
	 * @param text
	 * @param word
	 * @return wordPos
	 */
	private static WordPos getWordPos(List<String> text, int textLen, String word){
		
		
		List<Integer> list = new ArrayList<>();
		
		// ciclo da 1 a lunghezza testo
		IntStream.rangeClosed(1, textLen)
		// mapping parola testo - posizione
		.mapToObj(i -> new Pair<String,Integer>(text.get(i-1),i))
		.forEach(p -> {
			if(p.first().equals(word)){
				list.add(p.second());
			}
		});
		
		
		return new WordOcc(word,list);
	}
	
}
