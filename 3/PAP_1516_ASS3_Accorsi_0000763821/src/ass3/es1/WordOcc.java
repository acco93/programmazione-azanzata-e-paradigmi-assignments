package ass3.es1;

import java.util.List;

public class WordOcc implements WordPos{

	private List<Integer> pos;
	private String word;

	public WordOcc(String word, List<Integer> pos) {
		this.word = word;
		this.pos = pos;
	}

	@Override
	public String getWord() {
		return this.word;
	}

	@Override
	public List<Integer> getPos() {
		return this.pos;
	}

	@Override
	public String toString() {
		return "(" + word + "," + pos + ")";
	}
	
	

}
