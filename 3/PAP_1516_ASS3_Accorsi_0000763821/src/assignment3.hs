-- esercizio 3

data BSTree a = Nil | Node a (BSTree a) (BSTree a) deriving Show


bstMap :: (a -> b) -> BSTree a -> BSTree b
-- caso base
bstMap _ Nil = Nil
-- applico la funzione al valore e mi richiamo ricorsivamente sui figli sx e dx
bstMap f (Node a sx dx) = (Node (f a) (bstMap f sx) (bstMap f dx))

-- non è detto che mantenga la struttura di bst!!!!!!!!!! Avrei dovuto chiamare la funzione insert sotto ma avrei
-- dovuto portarmi dietro l'aggregato


-- fold pre order: elabora prima il sottoalbero sx poi la root ed infine quello dx.
-- In questo modo è possibile ottenere gli elementi ordinati
-- parametri:
-- funzione: aggregato -> corrente -> risultato
-- aggregato
-- albero
bstFold :: (b -> a -> b) -> b -> BSTree a -> b
-- caso base: ritorno il valore aggregato
bstFold _ a Nil = a
-- passo ricorsivo: elaboro sx -> current -> dx
bstFold f a (Node v sx dx) = let
  foldSx = bstFold f a sx
  foldCurrent = f foldSx v
  in
    bstFold f foldCurrent dx

-- implementazione della filter basata su bstFold
-- parametri: predicato & albero
bstFilter ::(Ord a) => (a -> Bool) -> BSTree a -> BSTree a
bstFilter _ Nil = Nil
-- chiamo la fold con:
-- funzione che aggiunge un nodo all'albero se soddisfa il predicato
-- aggregato iniziale, albero "vuoto"
-- albero da filtrare
bstFilter f tree = bstFoldInOrd (\t x -> if f x then insert t x else t) Nil tree where
  insert :: (Ord a) => BSTree a -> a -> BSTree a
  -- caso base: aggregato/sottoalbero senza alcun elemento
  insert Nil x = Node x Nil Nil
  insert (Node v sx dx) x
      -- nel caso di duplicati evito di aggiungere il nodo (per non pormi il problema di dove metterlo)
  	| v == x = Node v sx dx
  	| v  < x = Node v sx (insert dx x)
  	| v  > x = Node v (insert sx x) dx

-- Fold in order: utilizzata dalla bstFilter per creare un albero non totalmente sbilanciato (dato un albero non sbilanciato)
bstFoldInOrd :: (b -> a -> b) -> b -> BSTree a -> b
bstFoldInOrd _ a Nil = a
bstFoldInOrd f a (Node v sx dx) = let
  foldCurrent = f a v
  foldSx = bstFoldInOrd f foldCurrent sx
  in
    bstFoldInOrd f foldSx dx

bstForEach :: (a -> IO ()) -> BSTree a -> IO ()
bstForEach _ Nil = return ()
bstForEach f (Node v sx dx) = bstForEach f sx >> f v >> bstForEach f dx

intTree = Node 5 (Node 3 Nil (Node 4 Nil Nil)) (Node 7 (Node 6 Nil Nil) Nil)
listTree = Node 6 Nil (Node 7 Nil (Node 8 Nil (Node 9 Nil Nil)))


type Age = Int
data Person = Person String Age deriving (Show)

-- utilizzate nella insert della filter
instance Eq Person where
  (Person nameA ageA) == (Person nameB ageB) = nameA == nameB && ageA == ageB
  (Person nameA ageA) /= (Person nameB ageB) = nameA /= nameB || ageA /= ageB

instance Ord Person where
  (Person nameA ageA) < (Person nameB ageB) = nameA < nameB
  (Person nameA ageA) > (Person nameB ageB) = nameA > nameB

luca = Person "luca" 22
daniela = Person "daniela" 20
milena = Person "milena" 13
renza = Person "renza" 79
carlo = Person "carlo" 55

peopleTree = Node luca (Node carlo (Node daniela (Nil) (Nil)) (Nil)) (Node renza (Node milena (Nil) (Nil)) (Nil))

-- stampa in ordine crescente i nomi di tutte  le persone appartenenti all’albero passato come primo parametro,
-- la cui età sia compresa fra i valori specificati come secondo e terzo parametro.
--printPers :: BSTree Person -> Int -> Int -> IO()
printPers tree lb up = bstForEach (\(Person name _) -> print name) (bstFilter (\(Person _ age) -> age >= lb && age <= up) tree)
