package ass7.es1;

public class SetupMessage {

	private int minNum;
	private int maxNum;

	public SetupMessage(int minNum, int maxNum) {
		this.minNum = minNum;
		this.maxNum = maxNum;
	}

	public int getMinNum() {
		return minNum;
	}

	public int getMaxNum() {
		return maxNum;
	}

}
