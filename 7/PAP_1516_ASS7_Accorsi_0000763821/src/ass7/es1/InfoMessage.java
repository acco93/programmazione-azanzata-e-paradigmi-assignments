package ass7.es1;

public class InfoMessage {

	enum Info { WON, LOST}

	private Info info;
	
	public InfoMessage(Info info){
		this.info = info;
	}
	
	public Info get(){
		return this.info;
	}

	public static InfoMessage won() {
		return new InfoMessage(Info.WON);
	}

	public static InfoMessage lost() {
		return new InfoMessage(Info.LOST);
	}
}
