package ass7.es1;

public class GuessMessage {

	private final int value;

	public GuessMessage(int value){
		this.value = value;
	}
	
	public int get(){
		return this.value;
	}
	
	
}
