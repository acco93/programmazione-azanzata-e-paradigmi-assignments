package ass7.es2.concurrent;

/**
 * Possible events triggered by the model.
 * @author acco
 *
 */
public enum EventType {
	DISPLAY, FINISHED
}
