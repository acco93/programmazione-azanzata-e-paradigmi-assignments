package ass7.es2.concurrent;

/**
 * Possible event sources.
 * @author acco
 *
 */

public enum EventSourceType {
	ORACLE, PLAYER, SYSTEM
}
