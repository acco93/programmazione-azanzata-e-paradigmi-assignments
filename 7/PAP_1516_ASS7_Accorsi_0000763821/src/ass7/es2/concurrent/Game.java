package ass7.es2.concurrent;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * 
 * MVC model implementation.
 * 
 * @author acco
 *
 */

public class Game {
	
	private List<Player> players;
	private Set<EventListener> listeners;
	private int nPlayers;
	private SharedAppInfo appInfo;
	
	public Game(int nPlayers, SharedAppInfo appInfo) {
		this.listeners = new HashSet<>();
		this.nPlayers = nPlayers;
		this.appInfo = appInfo;
	}

	public void start() {
		
		SharedData sd = new SharedData(nPlayers);
		
		Oracle oracle = new Oracle(listeners, sd);
		oracle.start();
		
		players = new LinkedList<>();
		
		for (int i = 0; i < nPlayers; i++) {
			Player player = new Player("player" + i, oracle, appInfo, listeners, sd);
			players.add(player);
		}
		
		for(Player player : players){
			new Thread(player).start();
		}
		
	}

	public void resume() {
		for(Player player : players){
			player.resume();
		}
	}
	
	public void addEventListener(EventListener listener){
		this.listeners.add(listener);
	}
}
