package ass7.es2.concurrent;

import java.util.Optional;

/**
 * 
 * Mutable pair.
 * 
 * @author acco
 *
 * @param <T> first field type
 * @param <U> second field type
 */
public class MutablePair<T, U> {

	private Optional<T> first;
	private Optional<U> second;

	public MutablePair(T first, U second){
		this.first = Optional.ofNullable(first);
		this.second = Optional.ofNullable(second);
	}

	public MutablePair(T first){
		this(first, null);
	}
	
	public MutablePair(){
		this(null,null);
	}
	
	public Optional<T> getFirst() {
		return first;
	}

	public Optional<U> getSecond() {
		return second;
	}

	public void setFirst(T first) {
		this.first = Optional.ofNullable(first);
	}

	public void setSecond(U second) {
		this.second = Optional.ofNullable(second);
	}
	
	
	
	
}
