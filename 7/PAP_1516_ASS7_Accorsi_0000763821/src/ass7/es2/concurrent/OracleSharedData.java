package ass7.es2.concurrent;

/**
 * 
 * Oracle view on shared data object.
 * 
 * @author acco
 *
 */

public interface OracleSharedData {

	boolean reply(int magicNumber);
	
}
