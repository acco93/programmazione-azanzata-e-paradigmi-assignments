package ass7.es2.concurrent;

import javax.swing.JOptionPane;

/**
 * 
 * MVC controller implementation.
 * @author acco
 *
 */
public class Controller implements EventListener {
	
	private View view;
	private SharedAppInfo sd;
	private int nPlayers;
	private Game game;
	
	public Controller(){
		
		String input = JOptionPane.showInputDialog("How many players?");
		try{
			nPlayers = Integer.parseInt(input);
		} catch (NumberFormatException e){
			System.err.println(e);
			System.err.println("Default players number: 2");
			nPlayers = 2;
		}
		
		this.sd = new SharedAppInfo();
		this.game = new Game(nPlayers, sd);
		this.game.addEventListener(this);
		this.view = new FormView(this);
		
	}

	public SharedAppInfo getSharedInfo() {
		return sd;
	}

	public void pause() {
		this.sd.setPaused(true);
	}

	public void resume() {
		this.sd.setPaused(false);
		this.game.resume();
	}

	private void reset() {
		this.view.reset();
	}

	public void start() {
		this.game.start();
	}
	
	@Override
	public void notifyEvent(Event event) {
		switch(event.getType()){
		case DISPLAY:
			this.view.notifyEvent(event);
			break;
		case FINISHED:
			this.reset();
			break;
		default:
			break;
		
		}
		
	}


	
	

	
}
