package ass7.es2.concurrent;

public interface EventListener {

	/**
	 * Handle events coming from the model.
	 * @param event
	 */
	void notifyEvent(Event event);
	
}
