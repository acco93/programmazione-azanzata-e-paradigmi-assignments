package ass7.es2.oracleasmonitor;

/**
 * 
 * Application shared info.
 * 
 * @author acco
 *
 */
public class SharedInfo {

	private boolean isPaused;
	private boolean isFinished;
	
	public SharedInfo(){
		this.setPaused(false);
		this.setFinished(false);
	}

	public synchronized boolean isPaused() {
		return isPaused;
	}

	public synchronized void setPaused(boolean isPaused) {
		this.isPaused = isPaused;
	}

	public synchronized boolean isFinished() {
		return isFinished;
	}

	public synchronized void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}
	
	
	
}
