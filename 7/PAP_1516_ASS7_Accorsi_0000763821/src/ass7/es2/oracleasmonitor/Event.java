package ass7.es2.oracleasmonitor;

import java.time.LocalDateTime;

/**
 * Binding information between model/controller and view.
 * @author acco
 *
 */
public class Event {

	private LocalDateTime time;
	private String info;
	private EventSourceType source;
	private EventType type;

	public Event(String info, EventSourceType source, EventType type){
		this.time = LocalDateTime.now();
		this.info = info;
		this.source = source;
		this.type = type;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public String getInfo() {
		return info;
	}

	public EventSourceType getSource() {
		return source;
	}

	public EventType getType() {
		return type;
	}
	
	
	
}
