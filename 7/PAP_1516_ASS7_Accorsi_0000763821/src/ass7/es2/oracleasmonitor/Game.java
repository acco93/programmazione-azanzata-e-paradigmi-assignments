package ass7.es2.oracleasmonitor;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * 
 * MVC model implementation.
 * 
 * @author acco
 *
 */

public class Game {
	
	private List<Player> players;
	private Set<EventListener> listeners;
	private int nPlayers;
	private SharedInfo sd;
	
	public Game(int nPlayers, SharedInfo sd) {
		this.listeners = new HashSet<>();
		this.nPlayers = nPlayers;
		this.sd = sd;
	}

	public void start() {
		Oracle oracle = new Oracle(nPlayers, listeners);
		
		players = new LinkedList<>();
		
		for (int i = 0; i < nPlayers; i++) {
			Player player = new Player("player" + i, oracle, sd, listeners);
			players.add(player);
		}
		
		for(Player player : players){
			new Thread(player).start();
		}
		
	}

	public void resume() {
		for(Player player : players){
			player.resume();
		}
	}
	
	public void addEventListener(EventListener listener){
		this.listeners.add(listener);
	}
}
