package ass7.es2.oracleasmonitor;

/**
 * Possible event sources.
 * @author acco
 *
 */

public enum EventSourceType {
	ORACLE, PLAYER, SYSTEM
}
