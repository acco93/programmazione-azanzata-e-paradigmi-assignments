package ass7.es2.oracleasmonitor;

/**
 * Oracle possible replies to players guess attempt.
 * @author acco
 *
 */
public enum Info {
	WON, LOST, HIGHER, LOWER, GAME_COMPLETED
}
