package dishtype

object Type extends Enumeration {
  type Type = Value
  val MEAT, FISH, OTHER = Value
}