package dish
import dishtype.Type

class Dish (pName: String, pVegetarian: Boolean, pCalories: Int, pDishType: Type.Value) {
  private val name: String = pName
  private val vegetarian: Boolean = pVegetarian
  private val calories: Int = pCalories
  private val dishType: Type.Value = pDishType
  
  def getName = name
  def isVegetarian = vegetarian
  def getCalories = calories
  def getDishType = dishType
  override def toString = name+" "+vegetarian+" "+calories+" "+dishType
}
