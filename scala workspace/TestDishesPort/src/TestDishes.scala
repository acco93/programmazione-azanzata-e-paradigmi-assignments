import dish.Dish
import dishtype.Type

object TestDishes {
  def main(args: Array[String]): Unit = {
    val menu = List(new Dish("pork", false, 800, Type.MEAT),
      new Dish("beef", false, 700, Type.MEAT),
      new Dish("chicken", false, 400, Type.MEAT),
      new Dish("french fries", true, 530, Type.OTHER),
      new Dish("rice", true, 350, Type.OTHER),
      new Dish("season fruit", true, 120, Type.OTHER),
      new Dish("pizza", true, 550, Type.OTHER),
      new Dish("prawns", false, 400, Type.FISH),
      new Dish("salmon", false, 450, Type.FISH))

    // contare il numero di dish vegetariani
    println("Vegetarian dishes: " + menu.count { d => d.isVegetarian })

    // stampare in ordine crescente   
    // tutti i dish che hanno un numero di calorie comprese tra 400 e 600
    menu.filter { d => d.getCalories >= 400 && d.getCalories <= 600 }.sortWith((a, b) => a.getCalories <= b.getCalories).foreach { d => println(d.toString()) }

    // Dato un pasto formato dalle pietanze specificate in  myChoice (a seguire), 
    // determinare la quantità di calorie complessive del pasto
    val myChoice = List("chicken", "rice", "season fruit")
    
    println(menu.filter { d => myChoice.contains(d.getName) }.map { d => d.getCalories }.foldRight(0)((elem,acc)=>elem+acc));
    
  }
}