-- Esercizi Lab 1 rifatti a casa

-- Es 1
countElems :: [Int] -> Int -> Int
countElems [] _ = 0
countElems (x:xs) v
  | x == v  = 1 + rest
  | otherwise = rest
  where
    rest = countElems xs v

-- Es 2
countElemsWithPred :: [Int] -> (Int -> Bool) -> Int
countElemsWithPred [] _ = 0
countElemsWithPred (x:xs) p
  | p x = 1 + rest
  | otherwise = rest
  where
    rest = countElemsWithPred xs p

-- Es 3
data Elem = Dot | Star
countDots :: [Elem] -> Int
countDots [] = 0
countDots (Dot:xs) = 1 + countDots xs
countDots (_:xs) = countDots xs

-- Es 4
rev :: [a] -> [a]
rev [] = []
rev (x:xs) = rev xs ++ [x]

-- Es 5
--removeAll :: [a] -> a -> [a]
removeAll [] _ = []
removeAll (x:xs) e
  | x == e  = removeAll xs e
  | otherwise = [x] ++ removeAll xs e

-- Es 6
merge :: [Int] -> [Int] -> [Int]
merge [] [] = []
merge [] r = r
merge l [] = l
merge (x:xs) (y:ys)
  | x <= y  = [x] ++ merge xs (y:ys)
  | otherwise = [y] ++ merge ys (x:xs)

-- Es 7
data BST = Nil | Node Int BST BST
isPresentInBST :: Int -> BST -> Bool
isPresentInBST _ Nil = False
isPresentInBST v (Node x l r)
  | x == v  = True
  | v < x = isPresentInBST v l
  | otherwise = isPresentInBST v r

-- Tree: (Node 4 (Node 2 (Node 0 Nil Nil) (Node 3 Nil Nil))(Node 7 (Node 5 Nil Nil) Nil))

-- Es 8
data TBST = Nil | Node (String,Int) TBST TBST
buildBST :: [String] -> TBST
buildBST [] = Nil
buildBST (x:xs)
