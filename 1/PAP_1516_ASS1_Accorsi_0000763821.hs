-- Assignment #1

-- Es 1
isSorted :: [Int] -> Bool
-- considero la lista vuota ordinata
isSorted [] = True
-- lista con un elemento
isSorted [_] = True
-- una lista con più di un elemento ha sicuramente due elementi x e y e una coda potenzialmente vuota.
isSorted (x:y:xs)
  | x <= y  = isSorted (y:xs)
  | otherwise = False

-- Es 2
occSimple :: [String] -> String -> [Int]
occSimple l s = occSimpleLoop l s 0 where
  occSimpleLoop [] _ _ = []
  occSimpleLoop (x:xs) s i
    | x == s  = [i] ++ occSimpleLoop xs s (i+1)
    | otherwise = [] ++ occSimpleLoop xs s (i+1)

-- Es 3

-- data una lista ed un elemento, verifica se l'elemento è presente nella lista
has :: (Eq a) => [a] -> a -> Bool
has [] _ = False
has (x:xs) a
  | x == a    = True
  | otherwise = has xs a

-- data una lista, ritorna una lista nella quale non sono presenti duplicati
unique :: (Eq a) => [a] -> [a]
unique [] = []
unique (x:xs)
  | has xs x  = unique xs
  | otherwise = x : unique xs

occFull :: [String] -> [(String, [Int])]
occFull l = occFullLoop l (unique l) where
  occFullLoop _ [] = []
  -- scorro la lista degli elementi univoci (u:us) e richiamo occSimple
  -- passandogli la lista originale l e ciascun elemento della lista senza duplicati
  occFullLoop l (u:us)  = [(u,occSimple l u)] ++ occFullLoop l us

-- Es 4
data Sym = Dot | Dash
data Code = Single Sym | Comp Sym Code
countDash :: Code -> Int
countDash (Single Dash) = 1
countDash (Single Dot) = 0
countDash (Comp sym code) = countDash (Single sym) + countDash code

-- Es 5
showCode :: Code -> String
showCode (Single Dash) = "-"
showCode (Single Dot) = "."
-- concateno le porzioni di stringa
showCode (Comp sym code) = showCode (Single sym) ++ showCode code

-- funzioni utilizzate dagli esercizi 7, 8
-- funzione che data una tupla torna il primo elemento
first :: (a,b) -> a
first (a,_) = a
-- funzione che data una tupla torna il secondo elemento
second :: (a,b) -> b
second (_,b) = b

-- funzione che calcola la lunghezza di una lista
myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

-- funzione che dati due numeri binari di lunghezza ln1 e ln2 aggiunge al più
-- corto n zeri in testa dove n è la differenza di lunghezza tra il più lungo e il
-- più corto
align :: BNum -> BNum -> (BNum, BNum)
align n1 n2
  | ln1 < ln2 = (align ([Zero]++n1) n2)
  | ln1 > ln2 = (align n1 ([Zero]++n2))
  -- i numeri sono uguali => li ritorno
  | otherwise = (n1,n2)
  where
    ln1 = myLength n1
    ln2 = myLength n2

-- Es 6
data Digit = Zero | One
-- sinonimo
type BNum = [Digit]
-- definisco Eq e Show
instance Eq Digit where
  Zero == Zero  = True
  One == One  = True
  _ == _  = False

instance Show Digit where
  show Zero = "Zero"
  show One  = "One"


equalsBNum :: BNum -> BNum -> Bool
equalsBNum n1 n2 =
  let
    -- rendo i due numeri di uguale lunghezza
    nums = align n1 n2
    a = first nums
    b = second nums
    in computeEquals a b where
      computeEquals [] [] = True
      computeEquals [] [_] = False
      computeEquals [_] [] = False
      computeEquals (x:xs) (y:ys)
        | x == y  = equalsBNum xs ys
        | otherwise = False

-- alternativa Es 6 (sfruttando l'es 7)
-- converto i numeri in binario e faccio l'uguale -.-
equalsBNumAlt :: BNum -> BNum -> Bool
equalsBNumAlt n1 n2 = convBNum n1 == convBNum n2

-- Es 7
-- sfruttando lo shifting verso sinistra
convBNum :: BNum -> Int
convBNum n = binToDec n 0 where
  -- il numero binario è finito oppure è vuoto => torno l'accumulatore
  binToDec [] v = v
  binToDec (One:xs) v = let p = (2 * v + 1) in binToDec xs p
  binToDec (Zero:xs) v = let p = (2 * v) in binToDec xs p

-- alternativa meno efficiente
convBNumAlt :: BNum -> Int
convBNumAlt [] = 0
convBNumAlt (Zero:xs) = 0 * 2 ^ (myLength (xs)) + convBNumAlt xs
convBNumAlt (One:xs) = 1 * 2 ^ (myLength (xs)) + convBNumAlt xs

-- Es 8
-- implementa la somma dei numeri binari
-- approccio top-down: immagino di avere la somma per la coda, e mi chiedo
-- come combinarla per la testa
calcBNum :: BNum -> BNum -> Bool -> (BNum, Bool)
-- casi base
calcBNum [] [] _ = ([], False)
calcBNum [] n2 True = calcBNum [One] n2 False
calcBNum [] n2 False = (n2, False)
calcBNum n1 [] True = calcBNum n1 [One] False
calcBNum n1 [] False = (n1, False)
calcBNum [Zero] [Zero] False = ([Zero], False)
calcBNum [Zero] [Zero] True = ([One], False)
calcBNum [Zero] [One] False = ([One], False)
calcBNum [Zero] [One] True = ([Zero], True)
calcBNum [One] [Zero] False = ([One], False)
calcBNum [One] [Zero] True = ([Zero], True)
calcBNum [One] [One] False = ([Zero], True)
calcBNum [One] [One] True = ([One], True)
-- passo ricorsivo
calcBNum (x:xs) (y:ys) c =
  let
    -- effettuo la somma dei bit coda
    -- t è una tupla (numero binario, riporto)
    t = calcBNum xs ys c
    -- e di quelli in testa utilizzando come riporto quello tornato dai bit in coda
    h = calcBNum [x] [y] (second t)
    -- concateno la somma dei bit in testa con quella dei bit in coda e passo
    -- alla funzione principale l'eventuale riporto causato dai bit in testa
    in (first h ++ first t, second h)


sumBNum :: BNum -> BNum -> BNum
sumBNum n1 n2 =
  let
    -- rendo entrambi i numeri di lunghezza uguale, align restituisce una tupla
    nums = align n1 n2
    -- recupero i due numeri binari
    a = first nums
    b = second nums
    -- effettuo la somma settando inizialmente il riporto a false
    res = calcBNum a b False
    -- res è una tupla (somma, riporto)
    in if second res == True then
        -- nel caso ci sia riporto aggiungo 1 in testa
        [One] ++ first res
      else
        first res

-- Alternativa es 8
sumBNumAlt :: BNum -> BNum -> BNum
sumBNumAlt n1 n2 = decToBin( convBNum n1 + convBNum n2)

decToBin :: Int -> BNum
decToBin 0 = [Zero]
decToBin 1 = [One]
decToBin n =
  let
      -- calcolo il resto della divisione per 2
      r = rem n 2
      -- calcolo la divisione (intera) per 2
      d = quot n 2
  in
      if r == 1 then decToBin d ++ [One]
      else decToBin d ++ [Zero]

-- Es 9
data BTree a = Nil | Node a (BTree a) (BTree a)
countZeroInTree :: BTree Digit -> Int
countZeroInTree Nil = 0
countZeroInTree (Node Zero l r) = 1 + countZeroInTree l + countZeroInTree r
countZeroInTree (Node _ l r) = countZeroInTree l + countZeroInTree r

-- Es 10
getValuesLessThan :: BTree Int -> Int -> [Int]
getValuesLessThan Nil _ = []
getValuesLessThan (Node x l r) v
  -- può darsi abbia valori < anche a dx!
  | x < v = getValuesLessThan l v ++ [x] ++ getValuesLessThan r v
  -- in questo caso a dx ho sicuramente valori maggiori di v==x
  | x == v = getValuesLessThan l v
  -- è sbagliato, sta roba sotto è da cancellare! nn va mai a destra! deve andare a sx
  | otherwise = getValuesLessThan r v
