--[a,b,a,c] a -> [0,2]

let
	i = 0
	occSimple :: [String] -> String -> [Int]
	occSimple [] _ = []
	occSimple (x:xs) s
		| s == x	= [i] ++ occSimple xs s
		| otherwise	= occSimple xs s
in 
	occSimple ["aa","b","aa"] "aa"
