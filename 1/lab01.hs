-- Esercizio 1
countElems :: [Int] -> Int -> Int
countElems [] _ = 0
countElems (x:xs) v
	| x == v	= 1 + rest
	| otherwise = rest
	where
		-- salvo in una variabile locale
		rest = (countElems xs v)

-- Esercizio 2
countElemsWithPred :: [Int] -> (Int -> Bool) -> Int
countElemsWithPred [] _ = 0
countElemsWithPred (x:xs) p
	| p x	= 1 + countElemsWithPred xs p
	| otherwise = countElemsWithPred xs p

-- Esercizio 3
data Elem = Dot | Star
countDots :: [Elem] -> Int
countDots [] = 0
-- uso il pattern matching
countDots (Dot:xs) = 1 + countDots xs
countDots (_:xs) =  countDots xs

-- Esercizio 4
rev :: [a] -> [a]
rev [] = []
-- ++ serve a concatenare due liste
-- : serve a concatenare lista e valore
-- [x] lista che contiene x
rev (x:xs) = (rev xs) ++ [x]

-- Esercizio 5
-- removeAll :: [a] -> a -> [a]
removeAll [] _ = []
removeAll (x:xs) e
	| x == e = removeAll xs e
	| otherwise =  [x] ++ removeAll xs e

-- Esercizio 7
data BSTree = Nil | Node Int BSTree BSTree
isPresentInBST :: Int -> BSTree Int -> Bool
isPresentInBST _ Nil = False
-- l e r sono variabili del pattern che rappresentano il sx e dx
isPresentInBST v (Node x l r)
	| x == v = True
	| v < x = isPresentInBST v l
	| otherwise = isPresentInBST v r

testTree :: BSTree
testTree = (Node 10
					(Node 5
					 	(Node 1 Nil Nil)
						Nil)
						 (Node 13
						 	(Node 11 Nil Nil)
							 Nil))
