package ass5.es2.lambda;

public class Worker extends Thread{

	private String name;
	private Behavior behavior;
	private int iteration;
	private boolean verboseExecution;

	Worker(String name, Behavior behavior){
		this.name = name;
		this.behavior = behavior;
		this.iteration = 0;
		this.verboseExecution = false;
	}
	
	public void print(String msg){
		System.out.println("** "+iteration+" ** ["+name+"] "+msg);
	}

	public void info(String msg){
		if(verboseExecution){
			System.out.println("(info)["+name+"] "+msg);
		}
	}

	@Override
	public void run() {
		while(true){
			this.behavior.run();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.iteration++;
		}
	}

	public void setVerboseExecution(boolean verboseExecution) {
		this.verboseExecution = verboseExecution;
	}
	
	
}
