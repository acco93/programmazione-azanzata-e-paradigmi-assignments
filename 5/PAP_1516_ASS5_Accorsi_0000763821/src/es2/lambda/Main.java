package ass5.es2.lambda;

import java.util.concurrent.Semaphore;

public class Main {

	private static final int COUNTERS = 4;
	private static final int THREADS = 6;
	private static final int SEMAPHORES = 7;

	private static final int MUTEX = 1;
	private static final int EVENT = 0;

	private static final int W1 = 0, W2 = 1, W3 = 2, W4 = 3, W5 = 4, W6 = 5;
	private static final int C1 = 0, C2 = 1, C3 = 2, C4 = 3;
	private static final int S1 = 0, S2 = 1, S3 = 2, S4 = 3, S5 = 4, S6 = 5, S45 = 6;

	public static void main(String[] args) {

		boolean verbose = false;

		if (args.length > 0) {
			if (args[0].equals("-log")) {
				verbose = true;
			} else {
				System.err.println("Undefined flag: " + args[0]);
			}
		}

		Worker[] w = new Worker[THREADS];

		UnsafeCounter[] c = new UnsafeCounter[COUNTERS];
		for (int i = 0; i < COUNTERS; i++) {
			c[i] = new UnsafeCounter(0);
		}

		Semaphore[] s = new Semaphore[SEMAPHORES];

		// w1 WAITs until w6 prints c4
		// w6 prints c4 then SIGNALs w1
		s[S1] = new Semaphore(EVENT);
		// w1 increments c1 then SIGNALs w2 & w3
		// w2 & w3 WAITs until w1 increment c1
		s[S2] = new Semaphore(EVENT);
		s[S3] = new Semaphore(EVENT);
		// w2 & w3 increment c2 & c3 then SIGNAL w4 & w5
		// w4 & w4 WAIT until w2 & w4 increment c2 & c3 then print c2 & c3
		// then concurrently update c4 and SIGNAL w6
		s[S4] = new Semaphore(EVENT);
		s[S5] = new Semaphore(EVENT);
		s[S6] = new Semaphore(EVENT);
		s[S45] = new Semaphore(MUTEX);

		/*
		 * 
		 * Workers behavior defined using lambda expressions.
		 * 
		 * 
		 */

		// worker 1
		w[W1] = new Worker("w1", () -> {

			w[W1].info("Incrementing the counter");
			c[C1].inc();
			w[W1].info("Signaling w2");
			s[S2].release();
			w[W1].info("Signaling w3");
			s[S3].release();

			w[W1].info("Waiting for w6");

			try {
				s[S1].acquire();
			} catch (Exception e) {
				e.printStackTrace();
			}

		});

		// worker 2
		w[W2] = new Worker("w2", () -> {

			w[W2].info("Waiting for w1");
			try {
				s[S2].acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			w[W2].info("Incrementing the counter");
			c[C2].inc();

			w[W2].info("Signaling w4");
			s[S4].release();

		});

		// worker 3
		w[W3] = new Worker("w3", () -> {

			w[W3].info("Waiting for w1");
			try {
				s[S3].acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			w[W3].info("Incrementing the counter");
			c[C3].inc();

			w[W3].info("Signaling w5");
			s[S5].release();

		});

		// worker 4
		w[W4] = new Worker("w4", () -> {
			try {
				s[S4].acquire();
			} catch (Exception e) {
				e.printStackTrace();
			}

			w[W4].print("Counter value: " + c[C2].getValue());

			try {
				w[W4].info("Incrementing c4");
				s[S45].acquire();
				c[C4].inc();
				s[S45].release();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			w[W4].info("Signaling w6");
			s[S6].release();

		});

		// worker 5
		w[W5] = new Worker("w5", () -> {
			try {
				s[S5].acquire();
			} catch (Exception e) {
				e.printStackTrace();
			}

			w[W5].print("Counter value: " + c[C2].getValue());

			try {
				w[W5].info("Incrementing c4");
				s[S45].acquire();
				c[C4].inc();
				s[S45].release();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			w[W5].info("Signaling w6");
			s[S6].release();

		});

		// worker 6
		w[W6] = new Worker("w6", () -> {
			try {
				w[W6].info("Waiting for w4");
				s[S6].acquire();
				w[W6].info("Waiting for w5");
				s[S6].acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			w[W6].print("Counter value: " + c[C4].getValue());

			w[W6].info("Signaling w1");
			s[S1].release();
		});


		for(int i=0;i<THREADS;i++){
			w[i].setVerboseExecution(verbose);
		}

		
		for (int i = 0; i < THREADS; i++) {
			w[i].start();
		}

	}

}
