package ass5.es2.standard;

/**
 * 
 * Abstract worker. It defines the worker behavior.
 * 
 * @author acco
 *
 */

public abstract class Worker extends Thread {
	
	private int iteration;
	private String name;
	private boolean verboseExecution;
	
	Worker(String name){
		this.name = name;
		this.iteration=0;
		this.verboseExecution = false;
		System.out.println("Created "+ name+" ...");
	}
	
	protected synchronized void print(String message){
		System.out.println("** "+iteration+" ** ["+name+"] "+message);
	}
	
	protected synchronized void info(String message){
		if(verboseExecution){
			System.out.println("(info)["+name+"] "+message);
		}
	}
	
	@Override
	public void run() {
		while(true){
			this.action();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			this.iteration++;
		}
	}

	protected abstract void action();

	public void setVerboseExecution(boolean verboseExecution) {
		this.verboseExecution = verboseExecution;
	}
	
	
	
}
