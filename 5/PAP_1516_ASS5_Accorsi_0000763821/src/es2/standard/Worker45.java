package ass5.es2.standard;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Worker45 extends Worker {
	
	private Semaphore wait;
	private Semaphore signal;
	private Semaphore mutex;
	private UnsafeCounter cToPrint;
	private UnsafeCounter cToInc;


	
	public Worker45(String name, UnsafeCounter cToPrint, List<Semaphore> toWait, List<Semaphore> toSignal,
			Semaphore mutex, UnsafeCounter cToInc) {
		super(name);
		this.cToPrint = cToPrint;
		this.cToInc = cToInc;
		this.wait = toWait.get(0);
		this.signal = toSignal.get(0);
		this.mutex = mutex;

	}
	
	@Override
	public void action() {

		try {
			info("Waiting for w2/w3");
			this.wait.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		print("Counter value: "+cToPrint.getValue());
		
		try {
			info("Incrementing c4");
			this.mutex.acquire();
			this.cToInc.inc();		
			this.mutex.release();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		

		info("Signaling w6");
		this.signal.release();

	}

	
}
