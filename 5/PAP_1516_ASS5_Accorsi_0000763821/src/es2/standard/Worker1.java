package ass5.es2.standard;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Worker1 extends Worker {


	private UnsafeCounter counter;
	// to wait
	private Semaphore sW1;
	// to signal
	private Semaphore sW3;
	private Semaphore sW2;


	public Worker1(String name, UnsafeCounter c, List<Semaphore> toWait, List<Semaphore> toSignal) {
		super(name);
		this.counter = c;
		this.sW1 = toWait.get(0);
		this.sW2 = toSignal.get(0);
		this.sW3 = toSignal.get(1);
	}

	@Override
	public void action() {
		
		info("Incrementing the counter");
		this.counter.inc();
		
		info("Signaling w2");
		this.sW2.release();
		info("Signaling w3");
		this.sW3.release();
		
		try {
			info("Waiting for w6");
			this.sW1.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	
	
}
