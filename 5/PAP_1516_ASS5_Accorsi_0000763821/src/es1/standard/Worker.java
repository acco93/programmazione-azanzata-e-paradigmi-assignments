package ass5.es1.standard;

import java.util.List;


public class Worker extends Thread {

	private int lb;
	private int ub;
	private SharedData sd;

	public Worker(int lb, int ub, SharedData sd) {
		this.lb = lb;
		this.ub = ub;
		this.sd = sd;
	}

	@Override
	public void run() {

		List<Point> points = sd.getSpace().getPoints();
		
		// computing partial sum
		
		Pair<Double, Double> localSum = new Pair<>(0.0, 0.0);
		for (int i = lb; i < ub; i++) {
			double x = localSum.first() + points.get(i).getX();
			double y = localSum.second() + points.get(i).getY();
			localSum = new Pair<>(x, y);
		}

		sd.incSum(localSum);

		// waiting for the others
		// busy waiting ...

		while(!sd.sumDone()){}
		
		Point centroid = sd.getCentroid().get();

		Point localNearest = points.get(lb);

		// computing local nearest
		
		for (int i = lb + 1; i < ub; i++) {
			if (Space.d(points.get(i), centroid) < Space.d(localNearest, centroid)) {
				localNearest = points.get(i);
			}
		}

		// setting global nearest
		
		sd.setNearest(localNearest);

	}

}
