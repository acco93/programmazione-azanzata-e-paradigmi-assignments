package ass5.es1.standard;

import java.util.List;
import java.util.Optional;

public class Main {

	static Space space;
	
	public static void main(String[] args) {
			
		long startTime;
		long endTime;
		
		
		if(args.length>0){
			int n = Integer.parseInt(args[0]);
			space = new Space(n);
		} else {
			space = new Space();
		}
		
		
		System.out.println("== MULTI THREADED VERSION (NATIVE SYNC/CS) ==");
		
		startTime = System.currentTimeMillis();
		Main.multiThreadedVersion();
		endTime = System.currentTimeMillis();		
		System.out.println("\tElapsed time: "+ (endTime-startTime)+" ms");
		
		System.out.println("== SEQUENTIAL STREAM VERSION ==");
		startTime = System.currentTimeMillis();
		Main.sequentialStreamVersion();
		endTime = System.currentTimeMillis();
		System.out.println("\tElapsed time: "+ (endTime-startTime)+" ms");
	}

	private static void sequentialStreamVersion() {
		
		List<Point> points = space.getPoints();
		Pair<Double, Double> acc = new Pair<>(0.0,0.0);
		acc = points.stream()
				.map(p->new Pair<Double,Double>(p.getX(),p.getY()))
				.reduce(acc, (pSum, nElem)->new Pair<Double,Double>(pSum.first()+nElem.first(),pSum.second()+nElem.second()));
		
		Point centroid = new Point(acc.first()/points.size(), acc.second()/points.size());
		

		Optional<Point> nearest = points.stream().min((a,b)->Double.compare(Space.d(a,centroid), Space.d(b,centroid)));
		
		System.out.println("\tCentroid: "+ centroid);		
		System.out.println("\tNearest point: "+nearest.get());
		System.out.println("\tDistance: "+Space.d(nearest.get(), centroid));
		
	}

	private static void multiThreadedVersion() {

		final int procs = Runtime.getRuntime().availableProcessors() + 1;
							
		List<Point> points = space.getPoints();
		
		SharedData sd = new SharedData(procs, space);
		
		int pointsPerThread = points.size()/procs;
		
		// lower bound
		int lb = 0;
		// upper bound
		int ub = pointsPerThread;
		
		Worker[] w = new Worker[procs];

		for(int i=0;i<procs;i++){
			
			if(i==procs-1){
				ub = points.size();
			}
			
			w[i] = new Worker(lb, ub, sd);
			w[i].start();
			lb = ub;
			ub += pointsPerThread;
		}
		
		for(int i=0;i<procs;i++){
			try {
				w[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
				
		System.out.println("\tCentroid: "+ sd.getCentroid().get());
		System.out.println("\tNearest point: "+sd.getNearest().get());
		System.out.println("\tDistance: "+Space.d(sd.getNearest().get(),sd.getCentroid().get() ));
	}
	
}
