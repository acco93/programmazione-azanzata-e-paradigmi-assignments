package ass5.es1.semaphore;

import java.util.List;
import java.util.concurrent.Semaphore;

public class Worker extends Thread {

	private int lb;
	private int ub;
	private Semaphore mutex;
	private Semaphore[] toSignal;
	private Semaphore toWait;
	private SharedData sd;

	public Worker(int lb, int ub, SharedData sd, Semaphore mutex, Semaphore toWait, Semaphore[] toSignal) {
		this.lb = lb;
		this.ub = ub;
		this.sd = sd;
		this.mutex = mutex;
		this.toWait = toWait;
		this.toSignal = toSignal;
	}

	@Override
	public void run() {

		List<Point> points = sd.getSpace().getPoints();
		
		Pair<Double, Double> localSum = new Pair<>(0.0, 0.0);
		for (int i = lb; i < ub; i++) {
			double x = localSum.first() + points.get(i).getX();
			double y = localSum.second() + points.get(i).getY();
			localSum = new Pair<>(x, y);
		}

		try {

			mutex.acquire();
			sd.incSum(localSum);
			mutex.release();

		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// signal the others that I'm here...
		for (int i = 0; i < toSignal.length; i++) {
			toSignal[i].release();
		}

		// ... and wait for them
		try {
			for (int i = 0; i < toSignal.length; i++) {
				toWait.acquire();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// computing local distances

		Point centroid = sd.getCentroid().get();

		Point localNearest = points.get(lb);

		for (int i = lb + 1; i < ub; i++) {
			if (Space.d(points.get(i), centroid) < Space.d(localNearest, centroid)) {
				localNearest = points.get(i);
			}
		}

		try {
			mutex.acquire();
			sd.setNearest(localNearest);
			mutex.release();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
