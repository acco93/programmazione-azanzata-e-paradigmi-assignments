package ass5.es1.semaphore;

/**
 * 
 * A simple immutable 2d point.
 * 
 * @author acco
 *
 */

public class Point {

	private double x;
	private double y;

	Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}

}
