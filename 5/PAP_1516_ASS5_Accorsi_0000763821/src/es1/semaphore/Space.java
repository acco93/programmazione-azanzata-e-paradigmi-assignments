package ass5.es1.semaphore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 
 * A bi-dimensional space containing n (default: 20000000) points.
 * 
 * @author acco
 *
 */

public class Space {

	private static final int MX = 10000;
	private static final int MY = 10000;
	private static final int SEED = 5335;
	
	List<Point> points;

	Space(int n) {
		this.points = new ArrayList<Point>();
		Random r = new Random(SEED);
		System.out.print("Generating ("+n+") points...");
		//IntStream.range(0, n).forEach(i -> points.add(new Point(r.nextInt(MX), r.nextInt(MY))));
		for(int i=0;i<n;i++){
			points.add(new Point(r.nextInt(MX), r.nextInt(MY)));
		}
		System.out.println("DONE!");
	}

	Space() {
		this(20000000);
	}

	public List<Point> getPoints() {
		return this.points;
	}

	@Override
	public String toString() {
		return "2D Space containing " + this.points.size() + " points";
	}

	public static double d(Point a, Point b){
		return Math.sqrt((a.getX()-b.getX())*(a.getX()-b.getX())+(a.getY()-b.getY())*(a.getY()-b.getY()));
	}
	
}
