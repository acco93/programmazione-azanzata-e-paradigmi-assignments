package ass5.es1.semaphore;

/**
 * Generic pair class.
 * @author acco
 *
 * @param <T> type T
 * @param <U> type U
 */
public class Pair<T, U> {

	private T firstField;
	private U secondField;
	
	Pair (T firstField, U secondField){
		this.firstField = firstField;
		this.secondField = secondField;
	}
	
	public T first () { 
		return this.firstField;
	}
	
	public U second () {
		return this.secondField;
	}
	
	@Override
	public String toString() {
		return "Pair (" + firstField + ", " + secondField + ")";
	}

	
	
	
}
